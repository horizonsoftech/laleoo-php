<?php

class Main_manager extends CI_Model {

    public function __construct() {

        $this->load->database();
    }

    /*
     * Global Insert function 
     */

    public function insert($data, $table) {
        if ($this->db->insert($table, $data)) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * Global Update function with only "ID"
     */

    public function update($id, $data, $table) {

        $this->db->trans_start();

        $this->db->where('id', $id);

        $this->db->update($table, $data);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {

            return false;
        } else {

            return true;
        }
    }

    /*
     * Global Update function with any ID
     */

    public function update_by_other_id($col_name, $col_value, $data, $table) {

        $this->db->trans_start();

        $this->db->where($col_name, $col_value);

        $this->db->update($table, $data);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {

            return false;
        } else {

            return true;
        }
    }

    /*
     * Global Update function with only "ID"
     */

    public function updateTaskId($id, $data, $table) {

        $this->db->trans_start();

        $this->db->where('taskId', $id);

        $this->db->update($table, $data);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {

            return false;
        } else {

            return true;
        }
    }

    /*
     * Global Delete function With "ID"(ITs a global value which Currently we are not using but its for Future).
     * $id = ID in which We need to Delete
     * $table = Table Name
     */

    public function delete($id, $table) {

        $this->db->where('id', $id);

        if ($this->db->delete($table)) {

            return true;
        } else {

            return false;
        }
    }

    /*
     * Global Select function With "ID"
     * $table = Table Name
     */

    public function select_all($table) {



        $this->db->select("*")
                ->from($table);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            return $query->result_array();
        } else {

            return 0;
        }
    }
public function select($statusId) {
        $query = 'SELECT 
s.status_media
FROM user_status_media s
WHERE s.user_status_id_fk = ' . $statusId . '';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {

            return FALSE;
        }
    }
    #

    public function get_all_data($uid) {
        $query = "select u.*,c.*,p.*from users u left join card_data c on u.user_id = c.user_id left join promotions p on u.user_id=p.user_id where u.user_id=$uid";

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {

            return 0;
        }
    }

    public function getAllValidQuestions() {
        $query = 'SELECT question_id AS QuestionId, question AS Question from question where status = 1;';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {

            return 0;
        }
    }

    public function getUserDataByEmail($email) {
        $query = 'SELECT u.*, u.id AS userId from users u where u.email_address = "' . $email . '";';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {

            return 0;
        }
    }

    # get user data with status by email

    public function getUserDataWithStatusByEmail($email) {
        $query = 'SELECT 
u.id AS userId,
u.*,
us.id AS status_id,
us.*,
usm.id AS status_media_id,
usm.*
FROM users u
LEFT JOIN user_status us
ON us.user_id = u.id AND us.status = 1
LEFT JOIN user_status_media usm 
ON usm.user_status_id_fk = us.id AND usm.status = 1
 WHERE email_address = "' . $email . '" ORDER BY us.id DESC LIMIT 1';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {

            return 0;
        }
    }

    public function getUserVerify($cellNo) {
        $query = 'SELECT phone_number FROM users WHERE phone_number = "' . $cellNo . '"';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {

            return FALSE;
        }
    }

    public function getUserVerifyByMail($email) {
        $query = 'SELECT email_address FROM users WHERE email_address = "' . $email . '"';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {

            return FALSE;
        }
    }

    public function getUserDataByMobile($cellNo) {
        $query = 'SELECT * from users WHERE phone_number = "' . $cellNo . '"';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {

            return 0;
        }
    }

    public function getUniqueNumber($rand_taskId) {

        $query = 'SELECT id FROM users WHERE verification_code = "' . $rand_taskId . '"  LIMIT 1';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return true;
        } else {

            return false;
        }
    }

    public function checkVerifywithCellNmb($cellNo, $verifyCode) {
        $query = 'SELECT id FROM users WHERE phone_number = "' . $cellNo . '" AND verification_code = "' . $verifyCode . '"';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return true;
        } else {

            return false;
        }
    }

    public function checkLogin($email, $password) {
        $query = 'SELECT id FROM users WHERE email_address = "' . $email . '" AND password = "' . $password . '"   LIMIT 1';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return true;
        } else {

            return false;
        }
    }

    public function updateVerifyCode($id, $data, $table) {

        $this->db->trans_start();

        $this->db->where('phone_number', $id);

        $this->db->update($table, $data);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {

            return false;
        } else {

            return true;
        }
    }

    #dynamic select 

    public function selectByUserIdDesc($status_col, $status, $order_by_col, $order_by, $table) {
        $this->db->where($status_col, $status)
                ->order_by($order_by_col, $order_by);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function getPersonalStatus($userId) {
        $query = 'SELECT 
sl.*,
us.time_duration,
us.time_format
FROM status_list sl
INNER JOIN user_status us
ON sl.user_id = us.user_id
WHERE sl.user_id = ' . $userId . '
GROUP BY sl.user_status
ORDER BY sl.id DESC';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {
            $data = array();
            return $data;
        }
    }

    ## select by id

    public function select_by_id($id, $table) {
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_by_alias($alias) {
        $this->db->where('alias', $alias);
        $query = $this->db->get('users');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_by_email($email) {
        $this->db->where('email', $email);
        $query = $this->db->get("users");
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    ##count check specifically for delete check either value is present in other table

    public function delete_count($col, $col_val, $table) {
        $this->db->where($col, $col_val);
        $this->db->from($table);
        $count = $this->db->count_all_results();
        return $count;
    }

    public function delete_count_two_columns($col_one, $col_val_one, $col_two, $col_val_two, $table) {
        $this->db->where($col_one, $col_val_one);
        $this->db->where($col_two, $col_val_two);
        $this->db->from($table);
        $count = $this->db->count_all_results();
        return $count;
    }

    ##get all for specic define foreign key id 

    public function select_by_other_id($col, $col_val, $table) {
        $this->db->where($col, $col_val);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function select_by_two_id($col_one, $col_val_one, $col_two, $col_val_two, $table) {
        $this->db->where($col_one, $col_val_one);
        $this->db->where($col_two, $col_val_two);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        };
    }

    public function select_limit_status($limit, $table) {
        $this->db->select('*')
                ->from($table)
                ->where("status", 1)
                ->limit($limit)
                ->order_by("id", "desc");
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }
        }
        return $data;
    }

    public function select_is_active($table) {
        $this->db->select('*')
                ->from($table)
                ->where("is_activited", 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function record_count_join($table1, $table2, $col1, $col2) {
        $this->db->select('count(*) as total')
                ->from($table1)
                ->join($table2, $table1 . "." . $col1 . "=" . $table2 . "." . $col2, 'inner');
        $query = $this->db->get();
        $count = $query->row_array();
        return $count['total'];
    }

    public function delete_by_other_id($col, $col_val, $table) {
        $this->db->where($col, $col_val);
        if ($this->db->delete($table)) {
            return true;
        } else {
            return false;
        }
    }

    public function update_total_contact($user_id) {
        $query = $this->db->query("UPDATE `users` SET `total_contacts`= `total_contacts`-1 WHERE `id` = $user_id");
    }

//    public function getAllStatus() {
//        $query = $this->db->query('SELECT 
//us.*,
//usm.*
//FROM user_status us
//LEFT JOIN user_status_media usm
//ON usm.user_status_id_fk = us.id
//WHERE us.status = 1');
//        if($query->num_rows()>0){
//            return $query->result_array();
//        } else {
//            return 0;
//        }
//    }
//    public function getAllStatus() {
//        $query = $this->db->query('SELECT 
//us.*,
//usm.*
//FROM user_status us
//LEFT JOIN user_status_media usm
//ON usm.user_status_id_fk = us.id
//WHERE us.status = 1');
//        if($query->num_rows()>0){
//            return $query->result_array();
//        } else {
//            return 0;
//        }
//    }
//    public function getAllStatus($userId) {
//        $query = $this->db->query('SELECT 
//u.f_name,
//u.l_name,
//us.*,
//us.id as status_id,
//us.status_type AS status_Type,
//usm.*,
//usm.status_type AS media_type,
//usm.status AS media_status
//FROM user_status us
//LEFT JOIN user_status_media usm
//ON us.id = usm.user_status_id_fk AND usm.status = 1
//LEFT JOIN  users u
//ON u.id = us.user_id
//WHERE us.status = 1 AND us.user_id ='. $userId.'
//        ORDER BY us.id DESC');
//        if ($query->num_rows() > 0) {
//            return $query->result_array();
//        } else {
//            return 0;
//        }
//    }

    public function getAllStatus($userId) {
        $query = $this->db->query('SELECT 
u.f_name,
u.l_name,
us.*,
us.id AS status_id
FROM user_status us
LEFT JOIN  users u
ON u.id = us.user_id
WHERE us.status = 1 AND us.user_id =' . $userId . '
        ORDER BY us.id DESC');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function getAllStatusMedia($userId) {
        $query = $this->db->query('SELECT 
usm.* 
FROM user_status_media usm
WHERE usm.status = 1 AND usm.user_status_id_fk =' . $userId . ' ORDER BY usm.id DESC');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function getUserData($cellNo, $email) {
        $query = 'SELECT * FROM users WHERE phone_number = "' . $cellNo . '" OR email_address = "' . $email . '"';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {

            return FALSE;
        }
    }

    public function getStatus($statusId) {
        $query = 'SELECT 
us.*,
us.id AS status_id
FROM user_status us
LEFT JOIN  user_status_media usm
ON us.id = usm.user_status_id_fk
WHERE us.id =' . $statusId . '';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {

            return FALSE;
        }
    }

    public function delete_by_two_id($col1, $col1_val, $col2, $col2_val, $table) {
        $where = "$col1 = $col1_val AND $col2 = $col2_val";
        $this->db->where($where);
        if ($this->db->delete($table)) {
            return true;
        } else {
            return false;
        }
    }

    public function friendsStatus($userId) {
        $query = 'SELECT 
u.f_name,
u.l_name,
us.*,
us.id AS status_id
FROM invite_users i
LEFT JOIN users u 
ON u.id = i.invite_to
LEFT JOIN user_status us
ON i.invite_to = us.user_id
LEFT JOIN user_status_media usm
ON us.id = usm.user_status_id_fk
WHERE i.invite_by = ' . $userId . ' AND i.is_accept_invite = 1 GROUP BY us.id
ORDER BY us.id DESC';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {
            $data = array();
            return $data;
        }
    }

    public function friendssStatus($userId) {
        $query = 'SELECT 
u.f_name,
u.l_name,
us.*,
us.id AS status_id
FROM invite_users i
LEFT JOIN users u 
ON u.id = i.invite_by
LEFT JOIN user_status us
ON i.invite_by = us.user_id
LEFT JOIN user_status_media usm
ON us.id = usm.user_status_id_fk
WHERE i.invite_to = ' . $userId . ' AND i.is_accept_invite = 1 GROUP BY us.id
ORDER BY us.id DESC';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {

            $data = array();
            return $data;
        }
    }

    public function getLatestStatus($userId) {
        $query = $this->db->query('SELECT 
u.f_name,
u.l_name,
us.*,
us.id AS status_id
FROM user_status us
LEFT JOIN  users u
ON u.id = us.user_id
WHERE us.status = 1 AND us.user_id =' . $userId . '
        ORDER BY us.id DESC LIMIT 1');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function getLatestStatusWithMedia($userId) {
        $query = $this->db->query('SELECT 
us.id AS status_id,
u.f_name,
u.l_name,
us.status_type,
us.user_status,
us.status_title,
us.start_time,
us.time_duration,
us.is_available,
us.time_format,
us.status AS status_is_active,
us.is_expire,
us.created_at,
usm.id AS status_media_id,
usm.status_type AS status_media_type,
usm.status_media,
usm.thumbnail,
usm.added_at,
usm.status AS status_media_is_active,
usm.is_expire
FROM user_status us
LEFT JOIN  users u
ON u.id = us.user_id
LEFT JOIN user_status_media usm
ON usm.user_status_id_fk = us.id
WHERE us.status = 1 AND us.user_id =' . $userId . '
        ORDER BY us.id DESC LIMIT 1');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function getStatusByStatusId($statusId) {
        $query = $this->db->query('SELECT 
u.f_name,
u.l_name,
us.*,
us.id AS status_id
FROM user_status us
LEFT JOIN users u
ON u.id = us.user_id
WHERE us.status = 1 AND us.id ="' . $statusId . '"
 ORDER BY us.id DESC');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function friendsLatestStatus($userId) {
        $query = 'SELECT 
u.f_name,
u.l_name,
us.*,
us.id AS status_id
FROM invite_users i
LEFT JOIN users u 
ON u.id = i.invite_to
LEFT JOIN user_status us
ON i.invite_to = us.user_id
LEFT JOIN user_status_media usm
ON us.id = usm.user_status_id_fk
WHERE i.invite_by = ' . $userId . ' AND i.is_accept_invite = 1 GROUP BY us.id
ORDER BY us.id DESC LIMIT 1';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {
            $data = array();
            return $data;
        }
    }

    public function friendssLatestStatus($userId) {
        $query = 'SELECT 
u.f_name,
u.l_name,
us.*,
us.id AS status_id
FROM invite_users i
LEFT JOIN users u 
ON u.id = i.invite_by
LEFT JOIN user_status us
ON i.invite_by = us.user_id
LEFT JOIN user_status_media usm
ON us.id = usm.user_status_id_fk
WHERE i.invite_to = ' . $userId . ' AND i.is_accept_invite = 1 GROUP BY us.id
ORDER BY us.id DESC LIMIT 1';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {

            $data = array();
            return $data;
        }
    }

    public function AllfriendsLatestStatus($userId) {
        $query = 'SELECT
uss.f_name, 
uss.l_name,
us.id AS status_id, 
f.friend_id AS user_id,
us.status_type, 
us.user_status, 
us.status_title, 
us.start_time, 
us.time_duration, 
us.is_available, 
us.time_format, 
us.status, 
us.hide_from_user_id, 
us.is_expire
FROM users u
INNER JOIN friends f 
ON f.user_id = u.id
LEFT JOIN 
(SELECT * FROM users) AS uss
ON uss.id = f.friend_id
LEFT JOIN 
(SELECT * FROM `user_status` ORDER BY id DESC) us
ON f.friend_id = us.user_id
WHERE u.id = ' . $userId . '
GROUP BY us.user_id';

        $qr = $this->db->query($query);
        if ($qr->num_rows() > 0) {

            return $qr->result_array();
        } else {

            $data = array();
            return $data;
        }
    }
    public function getStatusWithMedia($statusId) {
        $query = $this->db->query('SELECT 
us.id AS status_id,
us.status_type,
us.user_status,
us.status_title,
us.start_time,
us.time_duration,
us.is_available,
us.time_format,
us.status AS status_is_active,
us.is_expire,
us.created_at,
usm.status_type AS status_media_type,
usm.status_media,
usm.thumbnail,
usm.added_at,
usm.status AS status_media_is_active,
usm.is_expire
FROM user_status us
LEFT JOIN user_status_media usm
ON usm.user_status_id_fk = us.id
WHERE us.status = 1 AND us.id ="'. $statusId .'"
 ORDER BY us.id DESC');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    public function getFriends($userId) {
        $query = $this->db->query('SELECT 
f.user_id,
f.friend_id,
u.f_name,
u.l_name,
u.image,
u.phone_number
FROM friends f
LEFT JOIN users u 
ON u.id = f.friend_id
WHERE f.user_id =' . $userId);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function getHiddenUserId($userId) {
        $query = $this->db->query('SELECT us.hide_from_user_id FROM user_status us
WHERE us.user_id = "' . $userId . '"
ORDER BY us.id DESC LIMIT 1');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function selectPersonalId($status, $userId) {
        $query = $this->db->query('SELECT id AS personal_id FROM status_list WHERE user_status = "' . $status . '" AND user_id =' . $userId);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    
    public function selectByStatusId($id) {
        $query = $this->db->query('SELECT 
u.f_name,
u.l_name,
us.*
FROM user_status us
LEFT JOIN users u 
ON u.id = us.user_id
WHERE us.id ='.$id);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    
    public function removeDeviceId($id) {
        $query = $this->db->query('UPDATE users u SET u.reg_id = "" WHERE id ='.$id);
//        echo $query;
        if ($query > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function updateByTwoColumns($colOne, $colOneVal, $colTwo, $colTwoVal, $data, $table) {

        $this->db->trans_start();

        $this->db->where($colOne, $colOneVal);

        $this->db->where($colTwo, $colTwoVal);

        $this->db->update($table, $data);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {

            return false;
        } else {

            return true;
        }
    }

}

?>