<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Web_services extends CI_Controller {

    /**

     * Index Page for this controller.

     *

     * Maps to the following URL

     * 		http://example.com/index.php/welcome

     * 	- or -

     * 		http://example.com/index.php/welcome/index

     * 	- or -

     * Since this controller is set as the default controller in

     * config/routes.php, it's displayed at http://example.com/

     *

     * So any other public methods not prefixed with an underscore will

     * map to /index.php/welcome/<method_name>

     * @see http://codeigniter.com/user_guide/general/urls.html

     */
    public function __construct() {



        parent::__construct();

        header("Content-Type: application/json");

        $this->load->model("main_manager");

        $this->load->library('twilio');

        $this->load->library('email');

        $this->load->helper('common_helper');
    }

    public function index() {

        //$this->load->view('welcome_message'); 

        echo 'Welcome to Laleoo';

        //echo phpinfo();
    }

    public function select_all_countries() {

        header("Content-Type: application/json");

        $country = $this->main_manager->select_all('country');

        if ($country == 0) {

            $final_data['status'] = '0';
        } else {

            for ($i = 0; $i < count($country); $i++) {

                $final_data['getcountriesResult']['countries'][$i]['code'] = $country[$i]['code'];

                $final_data['getcountriesResult']['countries'][$i]['countrycode'] = $country[$i]['countrycode'];

                $final_data['getcountriesResult']['countries'][$i]['countryname'] = $country[$i]['countryname'];

                $final_data['getcountriesResult']['countries'][$i]['gmt_zone'] = $country[$i]['gmt_zone'];

                $final_data['getcountriesResult']['countries'][$i]['image'] = $country[$i]['image'];
            }

            $final_data['status'] = '1';
        }

        echo json_encode($final_data);

        die();
    }

//    public function signUp() {
//        header("Content-Type: application/json");
//        $f_name = $this->input->post('f_name', TRUE);
//        $l_name = $this->input->post('l_name', TRUE);
//        $gender = $this->input->post('gender', TRUE);
//        $email = $this->input->post('email', TRUE);
//        $country = $this->input->post('country', TRUE);
//        $phone_num = $this->input->post('phone_num', TRUE);
//        $password = $this->input->post('password', TRUE);
//        $device_type = $this->input->post('device_type');
//        $udid = $this->input->post('udid');
//
//        $getUser = $this->main_manager->getUserVerify($phone_num);
//        //var_dump($getUser);
//        if ($getUser) {
//
//            /////// SEND This CODE TO cellNo $final_data['NoVerify'] = '1234';
//            $rand_taskerurl = rand(11111, 99999);
//            $is_unique = false;
//
//            while (!$is_unique) {
//                $result = $this->main_manager->getUniqueNumber($rand_taskerurl);
//                if ($result === false)   // if you don't get a result, then you're good
//                    $is_unique = true;
//                else                     // if you DO get a result, keep trying
//                    $rand_taskerurl = rand(11111, 99999);
//            }
//
//            $from = FROM;
//            $to = $phone_num;
//
//            $message = 'Your Verification number is ' . $rand_taskerurl;
//            $response = $this->twilio->sms($from, $to, $message);
//
//            if ($response->IsError) {
//                $final_data['status'] = '0';
//                $final_data['error'] = $response->ErrorMessage;
//                $final_data['error1'] = $to;
//                $final_data['error2'] = $phone_num;
//            } else {
//
//                $dataIns = array(
//                    'f_name' => $f_name,
//                    'l_name' => $l_name,
//                    'gender' => $gender,
//                    'email' => $email,
//                    'country' => $country,
//                    'phone_num' => $phone_num,
//                    'password' => md5($password),
//                    'device_type' => $device_type,
//                    'udid' => $udid
//                );
//
//                $this->main_manager->insert($dataIns, 'user');
//                $user_id = $this->db->insert_id();
//                $final_data['user_id'] = $user_id;
//                $final_data['status'] = '1';
//            }
//
//            //////// END HERE////////////////////////
//        } else {
//            $final_data['status'] = '0';
//            $final_data['error'] = $_POST;
//        }
//        echo json_encode($final_data);
//        die();
//    }



    public function signUp() {

//        
//        echo json_encode($_POST);
//        die();



        $f_name = $this->input->post('f_name');

        $l_name = $this->input->post('l_name');

        $gender = $this->input->post('gender');

        $email = $this->input->post('email');

        $country = $this->input->post('country');

        $country_code = $this->input->post('country_code');

        $phone_num = $this->input->post('phone_num');

        $password = $this->input->post('password');

        $device_type = $this->input->post('device_type');

        $udid = $this->input->post('regId');

//        $f_name = 'abc';
//        $l_name = 'efg';
//        $gender = 'Male';
//        $email = 'paa@age.com';
//        $country = 'Pakistan';
//        $country_code = "+92";
//        $phone_num = "3361242852";
//        $password = 'PP';
//        $device_type = 'ihpne';
//        $udid = '';

        $getUser = $this->main_manager->getUserData($phone_num, $email);



        if ($getUser) {

            if ($getUser[0]['email_address'] == $email) {

                $final_data['status'] = 0;

                $final_data['error'] = "Email Already Exist";
            }if ($getUser[0]['phone_number'] == $phone_num || $getUser[0]['phone_number'] == 0 . $phone_num) {

                $final_data['status'] = 0;

                $final_data['error'] = "Phone Number Already Exist";
            }if ($getUser[0]['email_address'] == $email && $getUser[0]['phone_number'] == $phone_num) {

                $final_data['status'] = 0;

                $final_data['error'] = "Data Already Exist";
            }

            //////// END HERE////////////////////////
        } else {





            $rand_taskerurl = rand(11111, 99999);

            $is_unique = false;



            while (!$is_unique) {

                $result = $this->main_manager->getUniqueNumber($rand_taskerurl);

                if ($result === false)   // if you don't get a result, then you're good
                    $is_unique = true;
                else                     // if you DO get a result, keep trying
                    $rand_taskerurl = rand(11111, 99999);
            }



            $from = FROM;

            $to = $country_code . $phone_num;



            $message = 'Your Verification number is ' . $rand_taskerurl;

            $response = $this->twilio->sms($from, $to, $message);



            if ($response->IsError) {

                $final_data['status'] = '0';

                $final_data['error'] = $response->ErrorMessage;

                $final_data['error1'] = $to;

                $final_data['error2'] = $country_code . $phone_num;
            } else {

                // $final_data['user_id'] = $user_id;

                $final_data['status'] = '2';

                $final_data['message'] = 'message sent';
            }





            $dataIns = array(
                'f_name' => $f_name,
                'l_name' => $l_name,
                'gender' => $gender,
                'email_address' => $email,
                'country' => $country,
                'country_code' => $country_code,
                'phone_number' => $phone_num,
                'password' => md5($password),
                'device_type' => $device_type,
                'reg_id' => $udid,
                'status' => '1',
                'payment_plan_id' => '1',
                'verification_code' => $rand_taskerurl,
                'created_at' => date("Y-m-d h:i:s")
            );









//            $dataIns = array(
//                'f_name' => 'abc',
//                'l_name' => 'xyz',
//                'gender' => 'male',
//                'email_address' => 'abc@gmail.com',
//                'country' => 'country',
//                'phone_number' => $phone_num,
//                'password' => md5('test'),
//                'device_type' => '$device_type',
//                'udid' => '$udid',
//                'status' => '1',
//                'payment_plan_id' => '1',
//                'created_at' => date("Y-m-d h:i:s")
//            );
//echo json_encode($dataIns);
            //die();

            $this->main_manager->insert($dataIns, 'users');

            $user_id = $this->db->insert_id();

            $data[0] = array(
                'user_id' => $user_id,
                'user_status' => "At the Park",
            );

            $data[1] = array(
                'user_id' => $user_id,
                'user_status' => "Relaxing",
            );

            $data[2] = array(
                'user_id' => $user_id,
                'user_status' => "At Work",
            );

            $data[3] = array(
                'user_id' => $user_id,
                'user_status' => "On Vacation",
            );

            for ($o = 0; $o < count($data); $o++) {

                $this->main_manager->insert($data[$o], 'status_list');
            }

//            echo $user_id;
        }

        header("Content-Type: application/json");

        echo json_encode($final_data);

        die();
    }

    public function security_verify() {

//        header("Content-Type: application/json");

        $verification_number = $this->input->post('verify_number', TRUE);

        $cellNo = $this->input->post('phone_num', TRUE);

//        $cellNo = '03123999335';
//        $verification_number = '71519';

        $checkVerifyCell = $this->main_manager->checkVerifywithCellNmb($cellNo, $verification_number);

//        print_r($checkVerifyCell);
//        die();

        if ($checkVerifyCell) {

            $dataUpd = array(
                'is_verify' => 1,
            );



            $this->main_manager->update($checkVerifyCell[0]["id"], $dataUpd, 'users');

            $final_data['user_data'] = $this->main_manager->select_by_id($checkVerifyCell[0]["id"], 'users');

            $final_data['status'] = '1';

            $final_data['is_verify'] = '1';
        } else {
            $this->main_manager->delete_by_other_id('phone_num', $cellNo, "users");
            $final_data['status'] = '0';
        }



        echo json_encode($final_data);

        die();
    }

    public function forget_password() {

        $post_type = $this->input->post('type');

//        $post_type = 'mobile';

        if ($post_type == 'mobile') {

            $phone_number = $this->input->post('phone_num');

//            $phone_number = '+923123999335';



            $getUser = $this->main_manager->getUserVerify($phone_number);

            if ($getUser) {

                $new_password = uniqid();



                $from = FROM;

                $to = $phone_number;



                $message = 'Your new password is ' . $new_password;

                $response = $this->twilio->sms($from, $to, $message);



                if ($response->IsError) {

                    $final_data['status'] = '0';

                    $final_data['error'] = $response->ErrorMessage;

                    $final_data['error1'] = $to;

                    $final_data['error2'] = $phone_num;
                } else {

//                $final_data['user_id'] = $user_id;

                    $final_data['status'] = '1';

                    $final_data['message'] = 'password sent';
                }



                $update_data = array(
                    'password' => md5($new_password)
                );

                $this->main_manager->update_by_other_id('phone_number', $phone_number, $update_data, 'users');

                $final_data['status'] = '1';

                $final_data['message'] = 'Password sent';
            } else {

                $final_data['status'] = '0';

                $final_data['message'] = 'Invalid number OR Number not exist';
            }
        } else {

            $email = $this->input->post('email');



            $getUser = $this->main_manager->getUserVerifyByMail($email);

            if ($getUser) {

                $new_password = uniqid();



                $config['mailtype'] = 'html';

                $this->email->initialize($config);

                $this->email->from('info@laleoo.com', 'Laleoo');

                $this->email->to($email);

                $this->email->subject('New Password');

                $html = '<p>Your new password is:"' . $new_password . '"</p>';

                $this->email->message($html);

                $this->email->send();



                $update_data = array(
                    'password' => md5($new_password)
                );

                $this->main_manager->update_by_other_id('email_address', $email, $update_data, 'users');



                $final_data['status'] = '1';

                $final_data['message'] = 'Password sent';
            } else {

                $final_data['status'] = '0';

                $final_data['message'] = 'Invalid number OR Number not exist';
            }
        }

        echo json_encode($final_data);

        die();
    }

    #old login service
//    public function login() {
////        header("Content-Type: application/json");
//        $email = $this->input->post('email');
//        $pwd = $this->input->post('password');
////        $email = 'sami.shoaib@live.com';
////        $pwd = '55a4e89911a6d';
////		$email = 'james@gmail.com';
////		$pwd = 'qwe123';
//
//        $checkLogin = $this->main_manager->checkLogin($email, md5($pwd));
//
//        if ($checkLogin) {
//            $getUserData = $this->main_manager->getUserDataByEmail($email);
//
//            $final_data['user_data'] = $getUserData;
//            $final_data['status'] = '1';
////            print_r($getUserData);
////            die();
//        } else {
//            $final_data['status'] = '0';
//        }
//
//        echo json_encode($final_data);
//        die();
//    }
    #new login service



    public function login() {

//        header("Content-Type: application/json");

        $email = $this->input->post('email');

        $pwd = $this->input->post('password');



//        $email = 'ab@live.com';
//        $pwd = '123';



        $checkLogin = $this->main_manager->checkLogin($email, md5($pwd));



        if ($checkLogin) {

            $getUserData = $this->main_manager->getUserDataWithStatusByEmail($email);



            $final_data['user_data'] = $getUserData[0];

            $final_data['status'] = '1';

//            d($final_data);
//            die();
        } else {

            $final_data['status'] = '0';
        }



        echo json_encode($final_data);

        die();
    }

    #1 Status webservice ...

    public function defaultStatus() {



        $userId = $this->input->post('userId');

        $statusType = $this->input->post('statusType');

        $status = $this->input->post('status');

        $statusTitle = $this->input->post('statusTitle');

        $startTime = $this->input->post('startTime');

        $timeDuration = $this->input->post('timeDuration');

        $isAvailable = $this->input->post('isAvailable');

        $timeFormat = $this->input->post('timeFormat');



//        $userId = 34;
//        $statusType = 'Personalize';
//        $status = '1';
//        $statusTitle = 'music';
//        $startTime = '3';
//        $timeDuration = '3';
//        $isAvailable = '2';
//        $timeFormat = '2';



        if ($_POST) {

            $data = array(
                'user_id' => $userId,
                'user_status' => $status,
                'status_title' => $statusTitle,
                'start_time' => $startTime,
                'time_duration' => $timeDuration,
                'is_available' => $isAvailable,
                'status_type' => $statusType,
                'time_format' => $timeFormat,
                'status' => 1
            );

            $this->main_manager->insert($data, 'user_status');

            $id = $this->db->insert_id();

            $finalData['data'] = $this->main_manager->select_by_id($id, "user_status");

//            d($finalData); die();

            $finalData['status'] = 1;
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    #2 Edit Status webservice ...

    public function editStatus() {



        $userId = $this->input->post('userId');

        $statusId = $this->input->post('statusId');

//        $statusType = $this->input->post('statusType');

        $status = $this->input->post('status');

        $statusTitle = $this->input->post('statusTitle');

        $startTime = $this->input->post('startTime');

        $timeDuration = $this->input->post('timeDuration');

        $isAvailable = $this->input->post('isAvailable');

        $timeFormat = $this->input->post('timeFormat');



//        $userId = '34';
//        $statusId = '481';
//        $status = 'Mobile Gaming';
//        $statusTitle = 'hi';
//        $startTime = '30/07/2015';
//        $timeDuration = '20';
//        $isAvailable = '0';
//        $timeFormat = 'hour';
//        print_r($_POST); die();

        if ($_POST) {

            $data = $this->main_manager->select_by_id($statusId, "user_status");

            if ($data) {

                $data = array(
                    'user_status' => $status,
                    'start_time' => $startTime,
                    'status_title' => $statusTitle,
                    'time_duration' => $timeDuration,
                    'is_available' => $isAvailable,
                    'time_format' => $timeFormat,
                    'status' => 1
                );

                $this->main_manager->update($statusId, $data, 'user_status');

                $finalData['data'] = $this->main_manager->select_by_id($statusId, "user_status");

                $finalData['status'] = 1;
            } else {

                $finalData['message'] = "Status does not exist";
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    #3 Delete status webservice ...

    public function deleteStatus() {



        $statusId = $this->input->post('statusId');

//        $statusId = "2";

        if ($statusId) {

            $data = $this->main_manager->select_by_id($statusId, "user_status");

            if ($data) {

                $data = array(
                    'status' => 0
                );

                $this->main_manager->update($statusId, $data, 'user_status');

//            $this->main_manager->update($statusId, $data, 'user_status_media');

                $finalData['status'] = 1;
            } else {

                $finalData['message'] = 'Status does not exist';
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    #4 Upload status media webservice ...

    public function uploadStatusMedia() {

//        define('UPLOAD_DIR', $_SERVER['DOCUMENT_ROOT'] . '/demo/laleoo/assets/');

        $userId = $this->input->post('userId');

        $statusId = $this->input->post('statusId');

        $time = date("Y-m-d h:i:s");

        $finalData['status'] = 0;

//        $userId = "2";
//        $statusId = "2";
//
//        $statusMedia[0] = "iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKBAMAAABlIDIAAAAAIVBMVEUAAACgoKCgoKDd3d3Z2dnh4eHs7Ozx8fHm5ubc3NzT09OH3f/7AAAAAnRSTlMAtyOW6xcAAAAsSURBVAjXYxBiYGBQVwDiIgYGtTQFBk3TJgYV5yAGZWMFBhUnBgYtoBxQDQBcfARwK7k9TQAAAABJRU5ErkJggg==";
//        $statusMedia[1] = "iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKBAMAAABlIDIAAAAAIVBMVEUAAACgoKCgoKDd3d3Z2dnh4eHs7Ozx8fHm5ubc3NzT09OH3f/7AAAAAnRSTlMAtyOW6xcAAAAsSURBVAjXYxBiYGBQVwDiIgYGtTQFBk3TJgYV5yAGZWMFBhUnBgYtoBxQDQBcfARwK7k9TQAAAABJRU5ErkJggg==";



        $statusMedia = $this->input->post('statusMediaImage');

        if ($statusMedia) {

            for ($i = 0; $i < count($statusMedia); $i++) {

                #uploading image

                ignore_user_abort(true);

                $fileName = uniqid();

                $file = base64_decode($statusMedia[$i]);

                $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";

                $success = file_put_contents($fullPath, $file);

                $pro_image = $fileName . ".png";

                set_time_limit(0);



                $data = array(
                    'user_status_id_fk' => $statusId,
//                'status_type' => $mediaType,
                    'status_media' => $pro_image,
                    'added_at' => $time,
                    'status' => 1,
                );

                $data1 = $this->main_manager->insert($data, 'user_status_media');
            }

            if ($data1 == true) {

                $finalData['status'] = 1;
            } else {

                $finalData['error'] = "error";
            }
        }

        if (isset($_FILES) && !empty($_FILES['statusMediaVideo'])) {

            $thumbnail = $this->input->post('thumbnail');

            $statusMediaVideo = $_FILES['statusMediaVideo'];

            $files = $_FILES;

            $attachName = "statusMediaVideo";

            $count = count($_FILES['statusMediaVideo']['name']);

//            echo $count; die();

            for ($i = 0; $i < $count; $i++) {

                #uploading video

                $title = uniqid();

                $_FILES[$attachName]['name'] = $files[$attachName]['name'][$i];

                $_FILES[$attachName]['type'] = $files[$attachName]['type'][$i];

                $_FILES[$attachName]['tmp_name'] = $files[$attachName]['tmp_name'][$i];

                $_FILES[$attachName]['error'] = $files[$attachName]['error'][$i];

                $_FILES[$attachName]['size'] = $files[$attachName]['size'][$i];



                $fileName = $title . '_' . $_FILES[$attachName]['name'];

                $images[] = $fileName;

                $config['file_name'] = $fileName;



                $configVideo['upload_path'] = './assets/status_media/';

                $configVideo['allowed_types'] = '*';



                $this->load->library('upload', $configVideo);

                $this->upload->initialize($configVideo);

                if (!$this->upload->do_upload($attachName)) {

                    echo $this->upload->display_errors();
                } else {

                    $videoDetails = $this->upload->data();

                    $video_link = $videoDetails['file_name'];
                }

                #uploading thumbnail

                ignore_user_abort(true);

                $fileName = uniqid();

                $file = base64_decode($thumbnail[$i]);

                $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";

                $success = file_put_contents($fullPath, $file);

                $pro_image = $fileName . ".png";

                set_time_limit(0);

                $data2 = array(
                    'user_status_id_fk' => $statusId,
                    'status_media' => $video_link,
                    'thumbnail' => $pro_image,
                    'added_at' => $time,
                    'status' => 1,
                );



                $data3 = $this->main_manager->insert($data2, 'user_status_media');
            }



            if ($data3 == true) {

                $finalData['statusId'] = $statusId;

                $finalData['status'] = 1;
            } else {

                $finalData['error'] = "error";
            }
        }

        echo json_encode($finalData);
    }

    public function uploadStatusMediaOld() {

//        define('UPLOAD_DIR', $_SERVER['DOCUMENT_ROOT'] . '/demo/laleoo/assets/');

        $userId = $this->input->post('userId');

        $statusId = $this->input->post('statusId');

        $time = date("Y-m-d h:i:s");

        $finalData['status'] = 0;

//        $userId = "2";
//        $statusId = "2";
//
//        $statusMedia[0] = "iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKBAMAAABlIDIAAAAAIVBMVEUAAACgoKCgoKDd3d3Z2dnh4eHs7Ozx8fHm5ubc3NzT09OH3f/7AAAAAnRSTlMAtyOW6xcAAAAsSURBVAjXYxBiYGBQVwDiIgYGtTQFBk3TJgYV5yAGZWMFBhUnBgYtoBxQDQBcfARwK7k9TQAAAABJRU5ErkJggg==";
//        $statusMedia[1] = "iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKBAMAAABlIDIAAAAAIVBMVEUAAACgoKCgoKDd3d3Z2dnh4eHs7Ozx8fHm5ubc3NzT09OH3f/7AAAAAnRSTlMAtyOW6xcAAAAsSURBVAjXYxBiYGBQVwDiIgYGtTQFBk3TJgYV5yAGZWMFBhUnBgYtoBxQDQBcfARwK7k9TQAAAABJRU5ErkJggg==";



        $statusMedia = $this->input->post('statusMediaImage');

        if ($statusMedia) {

            for ($i = 0; $i < count($statusMedia); $i++) {

                #uploading image

                ignore_user_abort(true);

                $fileName = uniqid();

                $file = base64_decode($statusMedia[$i]);

                $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";

                $success = file_put_contents($fullPath, $file);

                $pro_image = $fileName . ".png";

                set_time_limit(0);



                $data = array(
                    'user_status_id_fk' => $statusId,
//                'status_type' => $mediaType,
                    'status_media' => $pro_image,
                    'added_at' => $time,
                    'status' => 1,
                );

                $data1 = $this->main_manager->insert($data, 'user_status_media');
            }

            if ($data1 == true) {

                $finalData['status'] = 1;
            } else {

                $finalData['error'] = "error";
            }
        }

        if (isset($_FILES) && !empty($_FILES['statusMediaVideo'])) {

            $thumbnail = $this->input->post('thumbnail');

            $statusMediaVideo = $_FILES['statusMediaVideo']['name'];

            $configVideo['upload_path'] = './assets/status_media/';

            $configVideo['allowed_types'] = '*';

            $video_name = $_FILES['statusMedia']['name'];

            $configVideo['file_name'] = $video_name;



            $this->load->library('upload', $configVideo);

            $this->upload->initialize($configVideo);

            if (!$this->upload->do_upload('statusMediaVideo')) {

                echo $this->upload->display_errors();
            } else {

                $videoDetails = $this->upload->data();

                $video_link = $videoDetails['file_name'];
            }

            #uploading thumbnail

            ignore_user_abort(true);

            $fileName = uniqid();

            $file = base64_decode($thumbnail);

            $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";

            $success = file_put_contents($fullPath, $file);

            $pro_image = $fileName . ".png";

            set_time_limit(0);

            $data2 = array(
                'user_status_id_fk' => $statusId,
                'status_media' => $video_link,
                'thumbnail' => $pro_image,
                'added_at' => $time,
                'status' => 1,
            );



            $data3 = $this->main_manager->insert($data2, 'user_status_media');
        }



        if ($data3 == true) {

            $finalData['statusId'] = $statusId;

            $finalData['status'] = 1;
        } else {

            $finalData['error'] = "error";
        }

        echo json_encode($finalData);
    }

    #5 Edit status media webservice ...

    public function editStatusMedia() {



        $userId = $this->input->post('userId');

        $statusId = $this->input->post('statusId');

        $mediaType = $this->input->post('mediaType');

        $thumbnail = $this->input->post('thumbnail');

        $time = date("Y-m-d h:i:s");

//        $userId = "2";
//        $statusId = "1";
//        $mediaType = "video";
//        $statusMedia = "ab.flv";
//        $thumbnail = "ab";

        if ($_POST) {

            $data = $this->main_manager->select_by_id($statusId, "user_status_media");

            if ($data) {

                if ($mediaType == "image") {

                    $statusMedia = $this->input->post('statusMedia');



                    ignore_user_abort(true);

                    $fileName = uniqid();

                    $file = base64_decode($statusMedia);

                    $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";

                    $success = file_put_contents($fullPath, $file);

                    $pro_image = $fileName . ".png";

                    set_time_limit(0);



                    $data = array(
                        'status_type' => $mediaType,
                        'status_media' => $pro_image,
                        'added_at' => $time,
                        'status' => 1,
                    );
                } else {

                    $statusMedia = $_FILES['statusMedia']['name'];

                    $configVideo['upload_path'] = './assets/status_media/';

                    $configVideo['allowed_types'] = '*';

                    $video_name = $_FILES['statusMedia']['name'];

                    $configVideo['file_name'] = $video_name;



                    $this->load->library('upload', $configVideo);

                    $this->upload->initialize($configVideo);

                    if (!$this->upload->do_upload('statusMedia')) {

                        echo $this->upload->display_errors();
                    } else {

                        $videoDetails = $this->upload->data();

                        $video_link = $videoDetails['file_name'];
                    }

                    if ($thumbnail) {

                        $data = array(
                            'status_type' => $mediaType,
                            'status_media' => $video_link,
                            'thumbnail' => $thumbnail,
                            'added_at' => $time,
                            'status' => 1,
                        );
                    } else {

                        $data = array(
                            'status_type' => $mediaType,
                            'status_media' => $video_link,
                            'added_at' => $time,
                            'status' => 1,
                        );
                    }
                }

                if ($this->main_manager->update($statusId, $data, 'user_status_media')) {

                    $mediaStatusId = $this->db->insert_id();

                    $finalData['statusId'] = $statusId;

                    $finalData['statusMediaId'] = $mediaStatusId;

                    $finalData['status'] = 1;
                }
            } else {

                $finalData['message'] = 'Status does not exist';
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    #6 Delete status media webservice ...

    public function deleteStatusMedia() {



        $statusId = $this->input->post('statusId');



        if ($statusId) {

            $data = $this->main_manager->select_by_id($statusId, "user_status_media");

            if ($data) {

                $data = array(
                    'status' => 0,
                );

                $this->main_manager->update($statusId, $data, 'user_status_media');

                $finalData['status'] = 1;
            } else {

                $finalData['message'] = 'Status does not exist';
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    public function array_count_values_of($value, $array) {

        $counts = array_count_values($array);

        return $counts[$value];
    }

    #7 Get all status webservice old...
//    public function getAllStatus() {
//
//        $userId = $this->input->post('userId');
////        $userId = 3;
//        if ($userId) {
//            $finalData['allStatus'] = $this->main_manager->getAllStatus($userId);
////         echo count($finalData['allStatus']);
////            die();
//
//            if ($finalData['allStatus']) {
//
//                for ($y = 0; $y < count($finalData['allStatus']); $y++) {
//                    if ($finalData['allStatus'][$y]['user_status_id_fk']) {
//                        @$count[$finalData['allStatus'][$y]['user_status_id_fk']] ++;
//                    }
//                }
//                for ($x = 0; $x < count($finalData['allStatus']); $x++) {
////           if($finalData['allStatus'][$x]['status_type'] == 'image'){
//                    $finalData['allStatus'][$x]['status_media'] = SITE_URL . "assets/status_media/" . $finalData['allStatus'][$x]['status_media'];
//                    $finalData['allStatus'][$x]['thumbnail'] = SITE_URL . "assets/thumbnail/" . $finalData['allStatus'][$x]['thumbnail'];
////           }  
//                    if ($finalData['allStatus'][$x]['user_status_id_fk']) {
//                        $finalData['allStatus'][$x]['media_count'] = $count[$finalData['allStatus'][$x]['user_status_id_fk']];
//                    } else {
//                        $finalData['allStatus'][$x]['media_count'] = 0;
//                    }
//                }
//
//                $finalData['status'] = 1;
//            } else {
//                $finalData['status'] = 0;
//            }
//        } else {
//            $finalData['status'] = 0;
//        }
//        echo json_encode($finalData);
//    }
    #7 Get all status new webservice ...



    public function getAllStatus() {

        $userId = $this->input->post('userId');
        $fData['friends_status'] = array();
        $finalData['allStatus'] = array();
        $data['allStatus2'] = array();
//        $userId = 42;

        if ($userId) {

            $data['allStatus2'] = $this->main_manager->getAllStatus($userId);
            $fData['friends_status'] = $this->main_manager->friendsStatus($userId);
            if (!empty($fData['friends_status'])) {
                $finalData['allStatus'] = array_merge($data['allStatus2'], $fData['friends_status']);
            } else {
                $finalData['allStatus'] = $data['allStatus2'];
            }

//             $count = count($fData['friends_status']);
//             for($f=0; $f<$count; $f++){
//                 $finalData['friends_status'][$f]['status_media'] = STATUS_MEDIA . $finalData['friends_status'][$f]['status_media'];
//             }
//             p($friendData);
//              die();
//              for($f=0; $f<count($friendData); $f++){
////                  p($friendData[$f]['invite_to']);
//             $finalData['friends_status'] = $this->main_manager->select_by_other_id("user_id",$friendData[$f]['invite_to'], "user_status");
//             
//             for($m=0; $m<count($finalData['friends_status']); $m++){
//                 p($finalData['friends_status'][$m]['id']);
//                 $finalData['friends_status_media'] = $this->main_manager->select_by_other_id("user_status_id_fk",$finalData['friends_status'][$m]['id'], "user_status_media");
//             }
//            }
//            p($finalData['friends_status']); echo "\n";
//             p($finalData['friends_status_media']); 
//            die();
            if ($finalData['allStatus']) {

                for ($x = 0; $x < count($finalData['allStatus']); $x++) {

                    $finalData['allStatus'][$x]['status_media'] = $this->main_manager->getAllStatusMedia($finalData['allStatus'][$x]['status_id']);

                    if ($finalData['allStatus'][$x]['status_media']) {



                        for ($z = 0; $z < count($finalData['allStatus'][$x]['status_media']); $z++) {

                            @$count[$finalData['allStatus'][$x]['status_media'][$z]['user_status_id_fk']] ++;

                            if ($finalData['allStatus'][$x]['status_media'][$z]['user_status_id_fk']) {

                                $finalData['allStatus'][$x]['media_count'] = $count[$finalData['allStatus'][$x]['status_media'][$z]['user_status_id_fk']];

                                $finalData['allStatus'][$x]['status_media'][0]['thumbnail_icon'] = $finalData['allStatus'][$x]['status_media'][$z]['thumbnail']; //SITE_URL . "assets/thumbnail/" . $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'];
                            } else {

                                $finalData['allStatus'][$x]['media_count'] = 0;
                            }

                            $finalData['allStatus'][$x]['status_media'][$z]['status_media'] = SITE_URL . "assets/status_media/" . $finalData['allStatus'][$x]['status_media'][$z]['status_media'];

                            if ($finalData['allStatus'][$x]['status_media'][$z]['thumbnail']) {

                                $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'] = SITE_URL . "assets/thumbnail/" . $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'];
                            } else {

                                $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'] = 0;
                            }
                        }
                    } else if (!$finalData['allStatus'][$x]['status_media']) {

                        $finalData['allStatus'][$x]['media_count'] = 0;
                    }
                }

                $finalData['status'] = 1;
            } else {

                $finalData['status'] = 0;
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    #8 Is available status webservice ...

    public function isAvailable() {

        $userId = $this->input->post('userId');

        $statusId = $this->input->post('statusId');

        $isAvailable = $this->input->post('isAvailable');

//        $userId = "34";
//        $isAvailable = "1";

        if ($_POST) {

            $data = $this->main_manager->select_by_other_id('id', $statusId, "user_status");

            if ($data) {

                $data = array(
                    'is_available' => $isAvailable,
                );

                $this->main_manager->update_by_other_id('id', $statusId, $data, 'user_status');

                $finalData['data'] = $this->main_manager->select_by_other_id('id', $statusId, "user_status");

                $finalData['status'] = 1;
            } else {

                $finalData['message'] = 'Status does not exist';
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    #9 personalize status webservice ..

    public function personalizeStatus() {

        $userId = $this->input->post('userId');

        $statusType = $this->input->post('statusType');

        $status = $this->input->post('status');

        $userId = '3';

        $statusType = 'personalize';

        $status = 'hello';

        if ($userId) {

            $data = array(
                'user_id' => $userId,
                'user_status' => $status,
                'status_type' => $statusType,
                'status' => 1
            );

            $this->main_manager->insert($data, 'user_status');

            $id = $this->db->insert_id();

            $finalData['data'] = $this->main_manager->select_by_id($id, "user_status");

            $finalData['status'] = 1;
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    #10 Create Status with media webservice .. 

    public function createStatus() {

        $userId = $this->input->post('userId');

        $statusType = $this->input->post('statusType');

        $status = $this->input->post('status');

        $statusTitle = $this->input->post('statusTitle');

        $startTime = $this->input->post('startTime');

        $timeDuration = $this->input->post('timeDuration');

        $isAvailable = $this->input->post('isAvailable');

        $timeFormat = $this->input->post('timeFormat');

        #status media parametters
//        define('UPLOAD_DIR', $_SERVER['DOCUMENT_ROOT'] . '/demo/laleoo/assets/');

        $statusId = $this->input->post('statusId');

        $time = date("Y-m-d h:i:s");

        $finalData['status'] = 0;

        $statusMedia = $this->input->post('statusMediaImage');

        $id = "";



//        $userId = 49;
//        $statusId = 914;
//        $statusType = 'Default';
//        $status = 'playing Game';
//        $statusTitle = 'music';
//        $startTime = '3';
//        $timeDuration = '3';
//        $isAvailable = '2';
//        $timeFormat = '2';



        if ($userId) {

            $data = array(
                'user_id' => $userId,
                'user_status' => $status,
                'status_title' => $statusTitle,
                'start_time' => $startTime,
                'time_duration' => $timeDuration,
                'is_available' => $isAvailable,
                'status_type' => $statusType,
                'time_format' => $timeFormat,
                'status' => 1
            );

            if ($status) {

                if (empty($statusId)) {

                    $this->main_manager->insert($data, 'user_status');

                    $id = $this->db->insert_id();

                    $finalData['data'] = $this->main_manager->select_by_id($id, "user_status");
                }
            }

//     -----------------------------------------------------------------------------
//            $statusMedia[0] = "iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKBAMAAABlIDIAAAAAIVBMVEUAAACgoKCgoKDd3d3Z2dnh4eHs7Ozx8fHm5ubc3NzT09OH3f/7AAAAAnRSTlMAtyOW6xcAAAAsSURBVAjXYxBiYGBQVwDiIgYGtTQFBk3TJgYV5yAGZWMFBhUnBgYtoBxQDQBcfARwK7k9TQAAAABJRU5ErkJggg==";
//            $statusMedia[1] = "iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKBAMAAABlIDIAAAAAIVBMVEUAAACgoKCgoKDd3d3Z2dnh4eHs7Ozx8fHm5ubc3NzT09OH3f/7AAAAAnRSTlMAtyOW6xcAAAAsSURBVAjXYxBiYGBQVwDiIgYGtTQFBk3TJgYV5yAGZWMFBhUnBgYtoBxQDQBcfARwK7k9TQAAAABJRU5ErkJggg==";



            if ($statusMedia) {

                for ($i = 0; $i < count($statusMedia); $i++) {

                    #uploading image

                    ignore_user_abort(true);

                    $fileName = uniqid();

                    $file = base64_decode($statusMedia[$i]);

                    $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";

                    $success = file_put_contents($fullPath, $file);

                    $pro_image = $fileName . ".png";

                    set_time_limit(0);

                    if ($statusId) {

                        $data = array(
                            'user_status_id_fk' => $statusId,
//                'status_type' => $mediaType,
                            'status_media' => $pro_image,
                            'added_at' => $time,
                            'status' => 1,
                        );
                    } else {

                        $data = array(
                            'user_status_id_fk' => $id,
//                'status_type' => $mediaType,
                            'status_media' => $pro_image,
                            'added_at' => $time,
                            'status' => 1,
                        );
                    }

                    $data1 = $this->main_manager->insert($data, 'user_status_media');
                }

                if ($data1 == true) {

//                    if ($statusId) {

                    if ($statusId) {

                        $finalData['data'] = $this->main_manager->select_by_id($statusId, "user_status");
                    } else {

                        $finalData['data'] = $this->main_manager->select_by_id($id, "user_status");
                    }

//                    if($statusId){
//                    $finalData['id'] = $statusId;
//                    } else {
//                        $finalData['id'] = $id;
//                    }
//                    echo "image status";

                    $finalData['status'] = 1;
                } else {

                    $finalData['error'] = "error";
                }
            }

            if (isset($_FILES) && !empty($_FILES['statusMediaVideo'])) {

//                print_r($_FILES); die();

                $thumbnail = $this->input->post('thumbnail');

                $statusMediaVideo = $_FILES['statusMediaVideo'];

                $files = $_FILES;

                $attachName = "statusMediaVideo";

                $count = count($_FILES['statusMediaVideo']['name']);

//            echo $count; die();

                for ($i = 0; $i < $count; $i++) {

                    #uploading video

                    $title = uniqid();

                    $_FILES[$attachName]['name'] = $files[$attachName]['name'][$i];

                    $_FILES[$attachName]['type'] = $files[$attachName]['type'][$i];

                    $_FILES[$attachName]['tmp_name'] = $files[$attachName]['tmp_name'][$i];

                    $_FILES[$attachName]['error'] = $files[$attachName]['error'][$i];

                    $_FILES[$attachName]['size'] = $files[$attachName]['size'][$i];



                    $fileName = $title . '_' . $_FILES[$attachName]['name'];

                    $images[] = $fileName;

                    $config['file_name'] = $fileName;



                    $configVideo['upload_path'] = './assets/status_media/';

                    $configVideo['allowed_types'] = '*';



                    $this->load->library('upload', $configVideo);

                    $this->upload->initialize($configVideo);

                    if (!$this->upload->do_upload($attachName)) {

                        echo $this->upload->display_errors();
                    } else {

                        $videoDetails = $this->upload->data();

                        $video_link = $videoDetails['file_name'];
                    }

                    #uploading thumbnail

                    ignore_user_abort(true);

                    $fileName = uniqid();

                    $file = base64_decode($thumbnail[$i]);

                    $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";

                    $success = file_put_contents($fullPath, $file);

                    $pro_image = $fileName . ".png";

                    set_time_limit(0);

                    if ($statusId) {

                        $data2 = array(
                            'user_status_id_fk' => $statusId,
                            'status_media' => $video_link,
                            'thumbnail' => $pro_image,
                            'added_at' => $time,
                            'status' => 1,
                        );
                    } else {

                        $data2 = array(
                            'user_status_id_fk' => $id,
                            'status_media' => $video_link,
                            'thumbnail' => $pro_image,
                            'added_at' => $time,
                            'status' => 1,
                        );
                    }

//                    var_dump($data2);
//                    die();

                    $data3 = $this->main_manager->insert($data2, 'user_status_media');
                }



                if ($data3 == true) {

//                    if ($statusId) {
//                        $finalData['data'] = $this->main_manager->getStatus($statusId);
//                    } else {
//                        $finalData['data'] = $this->main_manager->getStatus($id);
//                    }

                    if ($statusId) {

                        $finalData['data'] = $this->main_manager->select_by_id($statusId, "user_status");
                    } else {

                        $finalData['data'] = $this->main_manager->select_by_id($id, "user_status");
                    }

//                    echo "video status";

                    $finalData['status'] = 1;
                } else {

                    $finalData['error'] = "error";
                }
            }

//            -----------------------------------------------------

            $finalData['status'] = 1;
        } else {

//            echo "in else";

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    #11 Edit Status with media webservice ..

    public function editStatusWithMedia() {

        $userId = $this->input->post('userId');

        $statusId = $this->input->post('statusId');

        $statusType = $this->input->post('statusType');

        $status = $this->input->post('status');

        $statusTitle = $this->input->post('statusTitle');

        $startTime = $this->input->post('startTime');

        $timeDuration = $this->input->post('timeDuration');

        $isAvailable = $this->input->post('isAvailable');

        $timeFormat = $this->input->post('timeFormat');

        $statusMedia = $this->input->post('statusMediaImage');

        $time = date("Y-m-d h:i:s");

//        define('UPLOAD_DIR', $_SERVER['DOCUMENT_ROOT'] . '/demo/laleoo/assets/');
//        $userId = '34';
//        $statusId = '481';
//        $status = 'Mobile Gaming';
//        $statusTitle = 'hi';
//        $startTime = '30/07/2015';
//        $timeDuration = '20';
//        $isAvailable = '0';
//        $timeFormat = 'hour';
//        print_r($_POST); die();

        if ($_POST) {

            $data = $this->main_manager->select_by_id($statusId, "user_status");

            if ($data) {

                $data = array(
                    'user_status' => $status,
                    'start_time' => $startTime,
                    'status_title' => $statusTitle,
                    'time_duration' => $timeDuration,
                    'is_available' => $isAvailable,
                    'time_format' => $timeFormat,
                    'status' => 1
                );

                $this->main_manager->update($statusId, $data, 'user_status');

                $finalData['data'] = $this->main_manager->select_by_id($statusId, "user_status");

                $finalData['status'] = 1;
            } else {

                $finalData['message'] = "Status does not exist";
            }

//        -----------------------------------------------------------

            if ($statusMedia) {

                for ($i = 0; $i < count($statusMedia); $i++) {

                    #uploading image

                    ignore_user_abort(true);

                    $fileName = uniqid();

                    $file = base64_decode($statusMedia[$i]);

                    $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";

                    $success = file_put_contents($fullPath, $file);

                    $pro_image = $fileName . ".png";

                    set_time_limit(0);



                    $data = array(
                        'user_status_id_fk' => $statusId,
//                'status_type' => $mediaType,
                        'status_media' => $pro_image,
                        'added_at' => $time,
                        'status' => 1,
                    );



                    $data1 = $this->main_manager->insert($data, 'user_status_media');
                }

                if ($data1 == true) {

                    $finalData['data'] = $this->main_manager->select_by_id($statusId, "user_status");

                    $finalData['status'] = 1;
                } else {

                    $finalData['error'] = "error";
                }
            }

            if (isset($_FILES) && !empty($_FILES['statusMediaVideo'])) {



                $thumbnail = $this->input->post('thumbnail');

                $statusMediaVideo = $_FILES['statusMediaVideo'];

                $files = $_FILES;

                $attachName = "statusMediaVideo";

                $count = count($_FILES['statusMediaVideo']['name']);

//            echo $count; die();

                for ($i = 0; $i < $count; $i++) {

                    #uploading video

                    $title = uniqid();

                    $_FILES[$attachName]['name'] = $files[$attachName]['name'][$i];

                    $_FILES[$attachName]['type'] = $files[$attachName]['type'][$i];

                    $_FILES[$attachName]['tmp_name'] = $files[$attachName]['tmp_name'][$i];

                    $_FILES[$attachName]['error'] = $files[$attachName]['error'][$i];

                    $_FILES[$attachName]['size'] = $files[$attachName]['size'][$i];



                    $fileName = $title . '_' . $_FILES[$attachName]['name'];

                    $images[] = $fileName;

                    $config['file_name'] = $fileName;



                    $configVideo['upload_path'] = './assets/status_media/';

                    $configVideo['allowed_types'] = '*';



                    $this->load->library('upload', $configVideo);

                    $this->upload->initialize($configVideo);

                    if (!$this->upload->do_upload($attachName)) {

                        echo $this->upload->display_errors();
                    } else {

                        $videoDetails = $this->upload->data();

                        $video_link = $videoDetails['file_name'];
                    }

                    #uploading thumbnail

                    ignore_user_abort(true);

                    $fileName = uniqid();

                    $file = base64_decode($thumbnail[$i]);

                    $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";

                    $success = file_put_contents($fullPath, $file);

                    $pro_image = $fileName . ".png";

                    set_time_limit(0);



                    $data2 = array(
                        'user_status_id_fk' => $statusId,
                        'status_media' => $video_link,
                        'thumbnail' => $pro_image,
                        'added_at' => $time,
                        'status' => 1,
                    );



                    $data3 = $this->main_manager->insert($data2, 'user_status_media');
                }



                if ($data3 == true) {

                    $finalData['data'] = $this->main_manager->select_by_id($statusId, "user_status");

                    $finalData['status'] = 1;
                } else {

                    $finalData['error'] = "error";
                }
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    #12 Status expire webservice .. 

    public function expireStatus() {

        $userId = $this->input->post('userId');

        $statusId = $this->input->post('statusId');

//        $userId = 23;
//        $statusId =5; 

        $data = array(
            'is_expire' => 1,
        );

        if ($userId) {

            $data1 = $this->main_manager->update($statusId, $data, "user_status");

            $data2 = $this->main_manager->update_by_other_id('user_status_id_fk', $statusId, $data, 'user_status_media');

            if ($data1 == true && $data2 == true) {

                $finalData['status'] = 1;
            }
        }

        echo json_encode($finalData);
    }

    #13 Create Personal Status webservice ...

    public function createPersonalStatus() {

        $userId = $this->input->post('userId');

        $status = $this->input->post('personalStatus');

//        $userId = '3';
//        $status = 'hello';

        if ($userId) {

            $data = $this->main_manager->select_by_id($userId, 'users');

            if ($data) {

                $data = array(
                    'user_id' => $userId,
                    'user_status' => $status,
                );

                $this->main_manager->insert($data, 'status_list');

                $id = $this->db->insert_id();

                $finalData['personalId'] = $id;

                $finalData['personalStatus'] = $status;

                $finalData['status'] = 1;
            } else {

                $finalData['status'] = 0;
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    #14 Get Personal Status webservice ...

    public function getPersonalStatus($userId) {

//        $userId = 49;
//        $status = 'hello';
//        $finalData['status'] = 0;

        if ($userId) {

            $data = $this->main_manager->select_by_id($userId, 'users');

            if ($data) {

                $finalData['data'] = $this->main_manager->selectByUserIdDesc('user_id', $userId, 'id', 'desc', 'status_list');

                $finalData['status'] = 1;
            } else {

                $finalData['status'] = 0;
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    #14 Get Personal Status webservice ...

    public function getPersonals() {

        $userId = $this->input->post('userId');

//        $userId = 49;
//        $status = 'hello';
//        $finalData['status'] = 0;

        if ($userId) {

            $data = $this->main_manager->select_by_id($userId, 'users');

            if ($data) {

                $finalData['data'] = $this->main_manager->selectByUserIdDesc('user_id', $userId, 'id', 'desc', 'status_list');

                $finalData['status'] = 1;
            } else {

                $finalData['status'] = 0;
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    #15 delete Personal Status webservice ...

    public function deletePersonalStatus() {

        $userId = $this->input->post('userId');

        $id = $this->input->post('personalId');

//        $userId = 49;
//        $id = 23;

        $data = $this->main_manager->select_by_id($userId, 'users');

        if ($data) {

            $this->main_manager->delete_by_two_id('id', $id, 'user_id', $userId, 'status_list');

            $finalData['status'] = 1;
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    #16 Sync Contacts webservice ... 

    public function syncContacts() {

        $userId = $this->input->post('userId');

        $info = $this->input->post('info');



        // $userId = 39;
        // $info = '[{"contact_name":"Asad","number":"+923110253453"},{"contact_name":"Hshsjs","number":"03126467564"},{"contact_name":"Jjsja","number":"57664644694"},{"contact_name":"Jqoqoak","number":"64637755464"},{"contact_name":"Jsjjsjjssj","number":"56767545446"},{"contact_name":"Sami","number":"03123999335"}]';
//        p($info); die();

        $info = json_decode($info, true);



        $finalData = array();

        $data['data'] = $this->main_manager->select_all('users'); # get all user data
        //p($data); die();

        $finalData['data']['unregistered'] = array();

        $finalData['data']['registered'] = array();

        $totalCount = count($info);

        for ($i = 0; $i < $totalCount; $i++) { # info array loop
            if (substr($info[$i]['number'], 0, 1) == "+") { # finds + sign in numbers
                for ($x = 0; $x < count($data['data']); $x++) { # user data array loop
                    if (
                            $info[$i]['number'] == $data['data'][$x]['country_code'] . $data['data'][$x]['phone_number']        # first condition
                            || $info[$i]['number'] == $data['data'][$x]['country_code'] . 0 . $data['data'][$x]['phone_number'] # second condition
                    ) {



                        $finalData['data']['registered'][$i]['contact_name'] = $info[$i]['contact_name'];

                        $finalData['data']['registered'][$i]['number'] = $info[$i]['number'];



                        $reg_noo = str_replace($data['data'][$x]['country_code'], "", $info[$i]['number']);



                        $getData = $this->main_manager->select_by_other_id("phone_number", $reg_noo, "users");

                        $finalData['data']['registered'][$i]['user_id'] = $getData[0]['id'];

                        $finalData['data']['registered'][$i]['image'] = $getData[0]['image'];



                        $friend_status = 0;



                        $getFriend = $this->main_manager->select_by_two_id("invite_to", $getData[0]['id'], "invite_by", $userId, "invite_users");

                        if ($getFriend != 0) {

                            $friend_status = $getFriend[0]['is_accept_invite'];
                        }





                        $finalData['data']['registered'][$i]['friend'] = $friend_status;

                        unset($info[$i]);

                        break;
                    }
                } # end of user data array loop
            } else { # finds 0 in numbers
                for ($x = 0; $x < count($data['data']); $x++) { # user data array loop
                    if (
                            ($info[$i]['number'] == $data['data'][$x]['phone_number']        # first condition
                            || 0 . $info[$i]['number'] == $data['data'][$x]['phone_number'] # second condition
                            || $info[$i]['number'] == 0 . $data['data'][$x]['phone_number']) # third condition
                    ) {





                        $finalData['data']['registered'][$i]['contact_name'] = $info[$i]['contact_name'];

                        $finalData['data']['registered'][$i]['number'] = $info[$i]['number'];

                        $reg_noo = $finalData['data']['registered'][$i]['number'];

                        //echo substr($finalData['data']['registered'][$i]['number'],1);

                        if (substr($finalData['data']['registered'][$i]['number'], 0, 1) == 0) {

                            $reg_noo = substr($finalData['data']['registered'][$i]['number'], 1);
                        }



                        $getData = $this->main_manager->select_by_other_id("phone_number", $reg_noo, "users");

                        $finalData['data']['registered'][$i]['user_id'] = $getData[0]['id'];

                        $finalData['data']['registered'][$i]['image'] = $getData[0]['image'];



                        $friend_status = 0;



                        $getFriend = $this->main_manager->select_by_two_id("invite_to", $getData[0]['id'], "invite_by", $userId, "invite_users");

                        if ($getFriend != 0) {

                            $friend_status = $getFriend[0]['is_accept_invite'];
                        }





                        $finalData['data']['registered'][$i]['friend'] = $friend_status;



                        unset($info[$i]);

                        break;
                    }
                } # end of user data array loop
            }
        } #end of info array loop 

        $finalData['data']['registered'] = array_values($finalData['data']['registered']);

        $info = array_values($info);

        for ($x = $i; $x < count($finalData['data']['registered']); $x++) {

            for ($i = 0; $i < count($info); $i++) {

                if (@$info[$i]['number'] == $finalData['data']['registered'][$x]['number']) {

                    $finalData['data']['unregistered'][$i]['contact_name'] = $info[$i]['contact_name'];

                    $finalData['data']['unregistered'][$i]['number'] = $info[$i]['number'];
                }
            }
        }





        $finalData['data']['unregistered'] = array_merge($finalData['data']['unregistered'], $info);

        $finalData['data']['unregistered'] = array_values($finalData['data']['unregistered']);

//        for($y=0; $y){
//            
//        }

        echo json_encode($finalData);



        die();
    }

    #17 Send Friend Request webservice ... 

    public function sendFriendReq() {

        $requestBy = $this->input->post('reqBy');

        $requestTo = $this->input->post('reqTo');
        $user = $this->main_manager->select_by_id($requestBy, "users");
        //$requestBy = 70;
        //$requestTo = '[{"frnd_id":"39"}]';

        $requestTo = json_decode($requestTo, true);

//        p($requestTo);
//         die();

        $registrationIds = array();

        for ($a = 0; $a < count($requestTo); $a++) {

            $data = array(
                'invite_by' => $requestBy,
                'invite_to' => $requestTo[$a]['frnd_id'],
                'invite_send_at' => date("Y-m-d H:i:s"),
                'is_accept_invite' => 2
            );



            $dataIns = $this->main_manager->insert($data, "invite_users");

            //echo $requestTo[$a]['frnd_id'];

            $getData = $this->main_manager->select_by_other_id("id", $requestTo[$a]['frnd_id'], "users");

            //p($getData);
            // die();

            $registrationIds[$a] = $getData[0]['reg_id'];
        }

        //p($registrationIds);
        //die();
//        define('API_ACCESS_KEY', 'AIzaSyA2EwoPIJXUBvVyYacZksGcE87qZrvVdbo');



        $fields = array
            (
            'registration_ids' => $registrationIds,
            'content_available' => true,
            'notification' => array(
                "body" => $user[0]['f_name'] . "would like to add you as a friend.",
                "title" => "Friend Request",
                'sound' => "default"
            )
        );



        $headers = array
            (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );





        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

        // curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);

        curl_close($ch);

        // }  // Ending For Loop



        /* Sending Notification End */

        if ($result === FALSE) {

            // echo $result;
            // die('Curl failed: ' . curl_error($ch));
        } else {

            //echo $result;
        }



        $finalData['status'] = 1;



        echo json_encode($finalData);
    }

    #18 See friend requests webservice ... 

    public function friendList() {
        $userId = $this->input->post('userId');
//        $userId = 41;  
//        $users[0]['image'] = "";
        $data['data'] = $this->main_manager->select_by_other_id("invite_to", $userId, "invite_users");
        $users = $this->main_manager->select_by_id($data['data'][0]['invite_by'], "users");
//        p($users); die();
        $data['data'][0]['contact_name'] = $users[0]['f_name'] . $users[0]['l_name'];
        $data['data'][0]['user_id'] = $users[0]['id'];
        $data['data'][0]['image'] = $users[0]['image'];
        $data['status'] = 1;
//        p($data); die();
        echo json_encode($data);
        die();
    }

    #19 accept friend request webservice ...

    public function acceptRequest() {

        $requestTo = $this->input->post('reqTo');

        $requestBy = $this->input->post('reqBy');

//        $requestTo = 74;
//        $requestBy = 42;

        $data = $this->main_manager->select_by_two_id("invite_by", $requestBy, "invite_to", $requestTo, "invite_users");

        if ($data == 0) {

            $finalData['status'] = 0;

            die();
        } else {

            $dataIns = array(
                'is_accept_invite' => 1,
                'invite_accept_at' => date("Y-m-d H:i:s")
            );

            $this->main_manager->updateByTwoColumns("invite_by", $requestBy, "invite_to", $requestTo, $dataIns, "invite_users");
            $registrationIds = "";


            $getData = $this->main_manager->select_by_other_id("id", $requestBy, "users");
//
//            p($getData);
//             die();

            $registrationIds = $getData[0]['reg_id'];

            //p($registrationIds);
            //die();
//        define('API_ACCESS_KEY', 'AIzaSyA2EwoPIJXUBvVyYacZksGcE87qZrvVdbo');

            $fields = array
                (
                'registration_ids' => $registrationIds,
                'content_available' => true,
                'notification' => array(
                    "body" => "Need Body",
                    "title" => "Friend Request",
                    'sound' => "default"
                )
            );

            $headers = array
                (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

            // curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');

            curl_setopt($ch, CURLOPT_POST, true);

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);

            curl_close($ch);

            // }  // Ending For Loop



            /* Sending Notification End */

            if ($result === FALSE) {

                // echo $result;
                // die('Curl failed: ' . curl_error($ch));
            } else {

                //echo $result;
            }

            $finalData['status'] = 1;
        }

        echo json_encode($finalData);
    }

    #20 Cancel friend request webservice ...

    public function cancelRequest($requestTo, $requestBy) {

//        $requestTo = $this->input->post('reqTo');
//        $requestBy = $this->input->post('reqBy');
//        $requestTo = 40;
//        $requestBy = 24;

        $data = $this->main_manager->delete_by_two_id("invite_by", $requestBy, "invite_to", $requestTo, "invite_users");

        if ($data == 0) {

            $finalData['status'] = 0;

            die();
        } else {

            $finalData['status'] = 1;
        }

        echo json_encode($finalData);
    }

    #21 decline request webservice ...

    public function declineRequest() {

        $requestTo = $this->input->post('reqTo');

        $requestBy = $this->input->post('reqBy');

//        $requestTo = 40;
//        $requestBy = 24;

        $data = $this->main_manager->delete_by_two_id("invite_by", $requestBy, "invite_to", $requestTo, "invite_users");

        if ($data == 0) {

            $finalData['status'] = 0;

            die();
        } else {

            $finalData['status'] = 1;
        }

        echo json_encode($finalData);
    }

    #22 upload profile image webservice ...

    public function uploadImage() {
        $userId = $this->input->post('userId');
//        $userId = 70;
        $proImage = $this->input->post('proImage');
//        $proImage = "iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKBAMAAABlIDIAAAAAIVBMVEUAAACgoKCgoKDd3d3Z2dnh4eHs7Ozx8fHm5ubc3NzT09OH3f/7AAAAAnRSTlMAtyOW6xcAAAAsSURBVAjXYxBiYGBQVwDiIgYGtTQFBk3TJgYV5yAGZWMFBhUnBgYtoBxQDQBcfARwK7k9TQAAAABJRU5ErkJggg==";
        if ($proImage) {
            #uploading image
            ignore_user_abort(true);
            $fileName = uniqid();
            $file = base64_decode($proImage);
            $fullPath = UPLOAD_DIR . "profile_images/" . $fileName . ".png";
            $success = file_put_contents($fullPath, $file);
            $pro_image = $fileName . ".png";
            set_time_limit(0);

            $data = array(
                'image' => $pro_image
            );
            $this->main_manager->update($userId, $data, 'users');
            $finalData['status'] = 1;
        } else {
            $finalData['status'] = 0;
        }
        echo json_encode($finalData);
    }

    public function deleteUser($userId) {

        $finalData['data'] = $this->main_manager->getAllStatus($userId);
        
        $finalData['friends'] = $this->main_manager->select_by_other_id("invite_by", $userId, "invite_users");
        if(!empty($finalData['friends'])){
           for($a=0; $a<count($finalData['friends']); $a++){
               $finalData['friends'] = $this->main_manager->delete_by_other_id("invite_by", $userId, "invite_users");
           } 
        }
        for ($i = 0; $i < count($finalData['data']); $i++) {
            $this->main_manager->delete_by_other_id('user_status_id_fk', $finalData['data'][$i]['id'], "user_status_media");

            $this->main_manager->delete_by_other_id('id', $finalData['data'][$i]['id'], "user_status");
        }

        $this->main_manager->delete_by_other_id('id', $userId, "users");
    }

    public function test() {

        ob_start();

//Get the ipconfig details using system commond

        system('ipconfig /all');



// Capture the output into a variable



        $mycom = ob_get_contents();



// Clean (erase) the output buffer



        ob_clean();



        $findme = "Physical";

//Search the "Physical" | Find the position of Physical text

        $pmac = strpos($mycom, $findme);



// Get Physical Address

        $mac = substr($mycom, ($pmac + 36), 17);

//Display Mac Address

        echo $mac;

        die();
    }

    public function registration() {

        header('Content-Type: application/json');

        $email = $this->input->post("email");

        $userName = $this->input->post("userName");

        $regId = $this->input->post("regId");

        $type = $this->input->post("type");

        $email = str_replace("%40", "@", $email);

//        $email = "a@a.com";
//        $userName = "A";
//        $regId = "fafsdfs";
//        $type = "Android";

        if ($email != "") {

            $checkemail = $this->main_manager->select_by_other_id("email", $email, "registration");

            if (!empty($checkemail)) {

                $typestrtolower = strtolower($type);

                $data = array(
                    'user_name' => $userName,
                    'reg_id' => $regId,
                    'type' => $type,
                );



                $this->main_manager->update_by_email($checkemail[0]['email'], $data, 'registration');

                $get_data['id'] = $checkemail[0]['id'];

                $get_data['user_name'] = $checkemail[0]['username'];

                $get_data['email'] = $checkemail[0]['email'];

                $get_data['reg_id'] = $regId;

                $get_data['status'] = 1;

                $this->send_notification();

                echo json_encode($get_data);
            } else {

                $data = array(
                    'user_name' => $userName,
                    'email' => $email,
                    'reg_id' => $regId,
                    'type' => $type,
                );

                if ($insert_data = $this->main_manager->insert($data, "registration")) {

                    $get = $this->main_manager->select_by_other_id("email", $email, "registration");

                    $get_data['id'] = $get[0]['id'];

                    $get_data['user_name'] = $get[0]['user_name'];

                    $get_data['email'] = $get[0]['email'];

                    $get_data['reg_id'] = $get[0]['reg_id'];

                    $get_data['status'] = 1;

                    $this->send_notification();

                    echo json_encode($get_data);
                }
            }
        }
    }

    #push notification function 

    public function send_notification() {

        $final_data['final_data'] = $this->main_manager->select_all("registration");

        /* Sending Notification Start */

//        define('API_ACCESS_KEY', 'AIzaSyA2EwoPIJXUBvVyYacZksGcE87qZrvVdbo');



        $reg_id_array = array();



        for ($i = 0; $i < count($final_data['final_data']); $i++) {

            $reg_id_array[$i] = $final_data['final_data'][$i]["reg_id"];
        }



        $registrationIds = $reg_id_array;





        $fields = array
            (
            'registration_ids' => $registrationIds,
            'content_available' => true,
            'notification' => array(
                "body" => "great match!",
                "title" => "Portugal vs. Denmark",
                'sound' => "default"
            )
        );



        $headers = array
            (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );





        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

        // curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);

        curl_close($ch);

        // }  // Ending For Loop



        /* Sending Notification End */

        if ($result === FALSE) {

            echo $result;

            die('Curl failed: ' . curl_error($ch));
        } else {

            echo $result;
        }
    }

    public function push() {



// API access key from Google API's Console
//        define('API_ACCESS_KEY', 'AIzaSyAemyOJj_gi5HBCKJW_AHfVntbfLmThFhE');

        $registrationIds = "APA91bHumRxROvh5m7JYEyrN74RfFcZmXYAJFsZb-LrWl_bvnSKdMwtFOOQPxnXHi6hr-N7D3TNPqJXzEyiYxGKDNB3WqvjPuLZsOdzwo9aQxQ4xXTme3QSzRxpE0lGuMs-XcJSeRoVP";

        $registrationId = json_encode($registrationIds);

// prep the bundle

        $msg = array
            (
            'message' => 'here is a message. message',
            'title' => 'This is a title. title',
            'subtitle' => 'This is a subtitle. subtitle',
            'tickerText' => 'Ticker text here...Ticker text here...Ticker text here',
            'vibrate' => 1,
            'sound' => 1,
            'largeIcon' => 'large_icon',
            'smallIcon' => 'small_icon'
        );

        $fields = array
            (
            'registration_ids' => $registrationIds,
            'data' => $msg
        );



        $headers = array
            (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );



        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);

        curl_close($ch);

        echo $result;
    }

    public function sendNotification($registrationIdsArray, $messageData) {

        $apiKey = "AIzaSyAemyOJj_gi5HBCKJW_AHfVntbfLmThFhE";



        $headers = array("Content-Type:" . "application/json", "Authorization:" . "key=" . $apiKey);

        $data = array(
            'data' => $messageData,
            'registration_ids' => $registrationIdsArray
        );



        $ch = curl_init();



        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_URL, "https://android.googleapis.com/gcm/send");

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));



        $response = curl_exec($ch);

        curl_close($ch);



        return $response;
    }

    public function sendFriendRequest() {

        $requestBy = $this->input->post('reqBy');

        $requestTo = $this->input->post('reqTo');

        //$requestTo = '[{"frnd_id":"40"},{"frnd_id":"41"},{"frnd_id":"42"},{"frnd_id":"43"},{"frnd_id":"44"}]';

        $requestTo = json_decode($requestTo, true);

        //p($requestTo);

        for ($a = 0; $a < count($requestTo); $a++) {

            $finalData['final_data'][$a] = $this->main_manager->select_by_id($requestTo[$a]['frnd_id'], "users");

            //p($finalData['final_data']);
        }

        //p($finalData);



        /* Sending Notification Start */

//        define('API_ACCESS_KEY', 'AIzaSyA2EwoPIJXUBvVyYacZksGcE87qZrvVdbo');



        $reg_id_array = array();



        for ($i = 0; $i < count($finalData['final_data']); $i++) {

            $reg_id_array[$i] = $finalData['final_data'][$i][0]["reg_id"];
        }

        //p($reg_id_array);
        // die();

        $registrationIds = $reg_id_array;





        $fields = array
            (
            'registration_ids' => $registrationIds,
            'content_available' => true,
            'notification' => array(
                "body" => "great match!",
                "title" => "Portugal vs. Denmark",
                'sound' => "default"
            )
        );



        $headers = array
            (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );





        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

        // curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);

        curl_close($ch);

        // }  // Ending For Loop



        /* Sending Notification End */

        if ($result === FALSE) {

            echo $result;

            die('Curl failed: ' . curl_error($ch));
        } else {

            echo $result;
        }
    }

}
