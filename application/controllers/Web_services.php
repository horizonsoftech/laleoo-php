<?php

defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('MAX_EXECUTION_TIME', 0);

class Web_services extends CI_Controller {

    /**

     * Index Page for this controller.

     *

     * Maps to the following URL

     * 		http://example.com/index.php/welcome

     * 	- or -

     * 		http://example.com/index.php/welcome/index

     * 	- or -

     * Since this controller is set as the default controller in

     * config/routes.php, it's displayed at http://example.com/

     *

     * So any other public methods not prefixed with an underscore will

     * map to /index.php/welcome/<method_name>

     * @see http://codeigniter.com/user_guide/general/urls.html

     */
    public function __construct() {
        parent::__construct();

        header("Content-Type: application/json");

        $this->load->model("main_manager");

        $this->load->library('twilio');

        $this->load->library('email');

        $this->load->helper('common_helper');
    }

    public function index() {

        //$this->load->view('welcome_message');

        echo 'Welcome to Laleoo';

        //echo phpinfo();
    }

    public function select_all_countries() {

        header("Content-Type: application/json");

        $country = $this->main_manager->select_all('country');

        if ($country == 0) {

            $final_data['status'] = '0';
        } else {

            for ($i = 0; $i < count($country); $i++) {

                $final_data['getcountriesResult']['countries'][$i]['code'] = $country[$i]['code'];

                $final_data['getcountriesResult']['countries'][$i]['countrycode'] = $country[$i]['countrycode'];

                $final_data['getcountriesResult']['countries'][$i]['countryname'] = $country[$i]['countryname'];

                $final_data['getcountriesResult']['countries'][$i]['gmt_zone'] = $country[$i]['gmt_zone'];

                $final_data['getcountriesResult']['countries'][$i]['image'] = $country[$i]['image'];
            }

            $final_data['status'] = '1';
        }

        echo json_encode($final_data);

        die();
    }

    public function signUp() {

        $f_name = $this->input->post('f_name');

        $l_name = $this->input->post('l_name');

        $gender = $this->input->post('gender');

        $email = $this->input->post('email');

        $country = $this->input->post('country');

        $country_code = $this->input->post('country_code');

        $phone_num = ltrim($this->input->post('phone_num'), 0);

        $password = $this->input->post('password');

        $device_type = $this->input->post('device_type');

        $udid = $this->input->post('regId');

//        $f_name = 'abc';
//        $l_name = 'efg';
//        $gender = 'Male';
//        $email = 'paa@age.com';
//        $country = 'Pakistan';
//        $country_code = "+92";
//        $phone_num = "3361242852";
//        $password = 'PP';
//        $device_type = 'ihpne';
//        $udid = '';

        $getUser = $this->main_manager->getUserData($phone_num, $email);



        if ($getUser) {

            if ($getUser[0]['email_address'] == $email) {
                $getUser = $this->main_manager->getUserData($phone_num, $email);
                if ($getUser[0]['is_verify'] == 1) {
                    $final_data['status'] = 0;
                    $final_data['error'] = "Email Already Exist";
                }
            }if ($getUser[0]['phone_number'] == $phone_num || $getUser[0]['phone_number'] == 0 . $phone_num) {
                $getUser = $this->main_manager->getUserData($phone_num, $email);
                if ($getUser[0]['is_verify'] == 1) {
                    $final_data['status'] = 0;

                    $final_data['error'] = "Phone Number Already Exist";
                }
            }if ($getUser[0]['email_address'] == $email && $getUser[0]['phone_number'] == $phone_num) {
                $getUser = $this->main_manager->getUserData($phone_num, $email);
                if ($getUser[0]['is_verify'] == 1) {
                    $final_data['status'] = 0;

                    $final_data['error'] = "Data Already Exist";
                }
            }

            //////// END HERE////////////////////////
        } else {
            #verification code starts here
            $rand_taskerurl = rand(11111, 99999);

            $is_unique = false;
            while (!$is_unique) {

                $result = $this->main_manager->getUniqueNumber($rand_taskerurl);

                if ($result === false)   // if you don't get a result, then you're good
                    $is_unique = true;
                else                     // if you DO get a result, keep trying
                    $rand_taskerurl = rand(11111, 99999);
            }
//
//            $from = FROM;
//
//            $to = $country_code . $phone_num;
//
//            $message = 'Your Verification number is ' . $rand_taskerurl;
//
//            $response = $this->twilio->sms($from, $to, $message);
//
//            if ($response->IsError) {
//
//                $final_data['status'] = '0';
//
//                $final_data['error'] = $response->ErrorMessage;
//
//                $final_data['error1'] = $to;
//
//                $final_data['error2'] = $country_code . $phone_num;
//            } else {
//
//                 $final_data['user_id'] = $user_id;
//
            $final_data['status'] = '2';
//
            $final_data['message'] = 'message sent';
//            }
            #verification code ends here

            $dataIns = array(
                'f_name' => $f_name,
                'l_name' => $l_name,
                'gender' => $gender,
                'email_address' => $email,
                'country' => $country,
                'country_code' => $country_code,
                'phone_number' => $phone_num,
                'password' => md5($password),
                'device_type' => $device_type,
                'reg_id' => $udid,
                'status' => '1',
                'payment_plan_id' => '1',
                'verification_code' => $rand_taskerurl,
                'is_verify' => 1, #this is for testing. for signup without verification
                'created_at' => date("Y-m-d h:i:s")
            );

//            $dataIns = array(
//                'f_name' => 'abc',
//                'l_name' => 'xyz',
//                'gender' => 'male',
//                'email_address' => 'abc@gmail.com',
//                'country' => 'country',
//                'phone_number' => $phone_num,
//                'password' => md5('test'),
//                'device_type' => '$device_type',
//                'udid' => '$udid',
//                'status' => '1',
//                'payment_plan_id' => '1',
//                'created_at' => date("Y-m-d h:i:s")
//            );
//echo json_encode($dataIns);
            //die();
//            if (!$response->IsError) { # this code commented just for testing after test we have to uncomment this condition.
            $this->main_manager->insert($dataIns, 'users');
//            }
            $user_id = $this->db->insert_id();

            $data[0] = array(
                'user_id' => $user_id,
                'user_status' => "At the Park",
            );

            $data[1] = array(
                'user_id' => $user_id,
                'user_status' => "Relaxing",
            );

            $data[2] = array(
                'user_id' => $user_id,
                'user_status' => "At Work",
            );

            $data[3] = array(
                'user_id' => $user_id,
                'user_status' => "On Vacation",
            );
//            if (!$response->IsError) { # this comment is just for testing after test we have to remove comment from this condition
            for ($o = 0; $o < count($data); $o++) {

                $this->main_manager->insert($data[$o], 'status_list');
            }
        }
//            echo $user_id;
//        }

        header("Content-Type: application/json");

        echo json_encode($final_data);

        die();
    }

    public function security_verify() {

//        header("Content-Type: application/json");

        $verification_number = $this->input->post('verify_number', TRUE);

        $cellNo = $this->input->post('phone_num', TRUE);

//        $cellNo = '03123999335';
//        $verification_number = '71519';

        $checkVerifyCell = $this->main_manager->select_by_other_id('phone_number', $cellNo, "users");

//        p($checkVerifyCell);
//        die();

        if ($checkVerifyCell) {

            $dataUpd = array(
                'is_verify' => 1,
            );

            $this->main_manager->update($checkVerifyCell[0]["id"], $dataUpd, 'users');

            $final_data['user_data'] = $this->main_manager->select_by_id($checkVerifyCell[0]["id"], 'users');
//            p($final_data['user_data']); die();
            $final_data['status'] = '1';

            $final_data['is_verify'] = '1';
        } else {
            $this->main_manager->delete_by_other_id('phone_number', $cellNo, "users");
            $final_data['status'] = '0';
        }



        echo json_encode($final_data);

        die();
    }

    public function forget_password() {

        $post_type = $this->input->post('type');

//        $post_type = 'mobile';

        if ($post_type == 'mobile') {

            $phone_number = $this->input->post('phone_num');

//            $phone_number = '+923123999335';



            $getUser = $this->main_manager->getUserVerify($phone_number);

            if ($getUser) {

                $new_password = uniqid();



                $from = FROM;

                $to = $phone_number;



                $message = 'Your new password is ' . $new_password;

                $response = $this->twilio->sms($from, $to, $message);



                if ($response->IsError) {

                    $final_data['status'] = '0';

                    $final_data['error'] = $response->ErrorMessage;

                    $final_data['error1'] = $to;

                    $final_data['error2'] = $phone_num;
                } else {

//                $final_data['user_id'] = $user_id;

                    $final_data['status'] = '1';

                    $final_data['message'] = 'password sent';
                }



                $update_data = array(
                    'password' => md5($new_password)
                );

                $this->main_manager->update_by_other_id('phone_number', $phone_number, $update_data, 'users');

                $final_data['status'] = '1';

                $final_data['message'] = 'Password sent';
            } else {

                $final_data['status'] = '0';

                $final_data['message'] = 'Invalid number OR Number not exist';
            }
        } else {

            $email = $this->input->post('email');



            $getUser = $this->main_manager->getUserVerifyByMail($email);

            if ($getUser) {

                $new_password = uniqid();



                $config['mailtype'] = 'html';

                $this->email->initialize($config);

                $this->email->from('info@laleoo.com', 'Laleoo');

                $this->email->to($email);

                $this->email->subject('New Password');

                $html = '<p>Your new password is:"' . $new_password . '"</p>';

                $this->email->message($html);

                $this->email->send();



                $update_data = array(
                    'password' => md5($new_password)
                );

                $this->main_manager->update_by_other_id('email_address', $email, $update_data, 'users');



                $final_data['status'] = '1';

                $final_data['message'] = 'Password sent';
            } else {

                $final_data['status'] = '0';

                $final_data['message'] = 'Invalid number OR Number not exist';
            }
        }

        echo json_encode($final_data);

        die();
    }

    #new login service

    public function login() {

        $email = $this->input->post('email');

        $pwd = $this->input->post('password');

        $regId = $this->input->post('regId');
        $personalId = "";
//        $email = 'a@a.com';
//        $pwd = '123';

        $getData = $this->main_manager->select_by_other_id("email_address", $email, "users");
        if($getData == 0){
            echo json_encode(array(
                "status" => 0
            ));
            die();
        }

        #active status data
        $activeStatus = $this->main_manager->getLatestStatus($getData[0]['id']);
        if ($activeStatus) {
            $personalId = $this->main_manager->selectPersonalId($activeStatus[0]['user_status'], $activeStatus[0]['user_id']);
        }
//        p($personalId[0]);
        $finalData['activeStatus'] = $activeStatus[0];
        if ($finalData['activeStatus']) {
            if ($personalId != "") {
                $finalData['activeStatus']['personal_id'] = $personalId[0]['personal_id'];
            }

            $finalData['activeStatus']['status_media'] = $this->main_manager->getAllStatusMedia($activeStatus[0]['status_id']);

            $count = count($finalData['activeStatus']['status_media']);

            $finalData['activeStatus']['media_count'] = $count;
            for ($a = 0; $a < $count; $a++) {
                if ($finalData['activeStatus']['status_media'][$a]['status_media'] != "") {
                    $finalData['activeStatus']['status_media'][$a]['status_media'] = $finalData['activeStatus']['status_media'][$a]['status_media'];
                }
                if ($finalData['activeStatus']['status_media'][$a]['thumbnail'] != "") {
                    $finalData['activeStatus']['status_media'][$a]['thumbnail'] = $finalData['activeStatus']['status_media'][$a]['thumbnail'];
                }
            }
        }
        #friends data
        $finalData['friends'] = $this->main_manager->getFriends($getData[0]['id']);
        $countFrndData = COUNT($finalData['friends']);
        if ($finalData['friends']) {
            for ($f = 0; $f < $countFrndData; $f++) {
                $finalData['friends'][$f]['image'] = SITE_URL . "assets/profile_images/" . $finalData['friends'][$f]['image'];
            }
        }
        if (!empty($regId)) {
            if ($getData[0]['reg_id'] != $regId) {
                $data = array(
                    'reg_id' => $regId
                );

                $this->main_manager->update_by_other_id('email_address', $email, $data, "users");
            }
        }
        $checkLogin = $this->main_manager->checkLogin($email, md5($pwd));

        if ($checkLogin) {

            #user data
            $getUserData = $this->main_manager->getUserDataByEmail($email);

            $finalData['user_data'] = $getUserData[0];
            if ($finalData['user_data']['image'] != 0) {
                $finalData['user_data']['image'] = PROF_IMG . $getUserData[0]['image'];
            } else {
                $finalData['user_data']['image'] = 0;
            }
            $finalData['status'] = '1';
        } else {
            $finalData['status'] = '0';
        }

        echo json_encode($finalData);

        /********* pending friend requests notifications start *********/
        $registrationIds = array();
        $getInviteData = $this->main_manager->select_by_two_id("invite_to", $finalData['user_data']['id'], "pending_status", "1", "invite_users");
        $getMyUserData = $this->main_manager->select_by_other_id("id", $finalData['user_data']['id'], "users");
        //$getInviteData = $this->main_manager->select_by_other_id("invite_to", $finalData['user_data']['id'], "invite_users");
        array_push($registrationIds, $getMyUserData[0]['reg_id']);

        for ($a = 0; $a < count($getInviteData); $a++) {

            //echo $requestTo[$a]['frnd_id'];
            $getData = $this->main_manager->select_by_other_id("id", $getInviteData[$a]['invite_by'], "users");
            $this->main_manager->update($getInviteData[$a]['id'], array(
                "pending_status" => "0"
            ), "invite_users");

            /* Sending Notification Start */
            if(count($registrationIds) > 0){
                $fields = array(
                    'registration_ids' => $registrationIds,
                    'content_available' => true,
                    'notification' => array(
                        "title" => "Request Sent",
                        "body" => " would like to add you as a friend",
                        "sound" => "default"
                    )
                );

                $headers = array
                (
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                );


                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

                // curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');

                curl_setopt($ch, CURLOPT_POST, true);

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                $result = curl_exec($ch);

                curl_close($ch);

            }

        }
        /* pending friend requests notifications end */

        die();
    }

    #logout webservice

    public function logout() {
        $userId = $this->input->post('userId');
        $data = $this->main_manager->removeDeviceId($userId);
        //$this->main_manager->update($userId);
        if ($data == 1) {
            $finalData['status'] = 1;
        } else {
            $finalData['status'] = 0;
        }
        echo json_encode($finalData);
    }

    # Status webservice ...

    public function defaultStatus() {

        $userId = $this->input->post('userId');

        $statusType = $this->input->post('statusType');

        $status = $this->input->post('status');

        $statusTitle = $this->input->post('statusTitle');

        $startTime = $this->input->post('startTime');

        $timeDuration = $this->input->post('timeDuration');

        $isAvailable = $this->input->post('isAvailable');

        $timeFormat = $this->input->post('timeFormat');

//        $userId = 34;
//        $statusType = 'Personalize';
//        $status = '1';
//        $statusTitle = 'music';
//        $startTime = '3';
//        $timeDuration = '3';
//        $isAvailable = '2';
//        $timeFormat = '2';

        if ($_POST) {

            $data = array(
                'user_id' => $userId,
                'user_status' => $status,
                'status_title' => $statusTitle,
                'start_time' => $startTime,
                'time_duration' => $timeDuration,
                'is_available' => $isAvailable,
                'status_type' => $statusType,
                'time_format' => $timeFormat,
                'status' => 1
            );

            $this->main_manager->insert($data, 'user_status');

            $id = $this->db->insert_id();

            $finalData['data'] = $this->main_manager->select_by_id($id, "user_status");

            $finalData['status'] = 1;
        } else {
            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    # Edit Status webservice ...

    public function editStatus() {

        $userId = $this->input->post('userId');

        $statusId = $this->input->post('statusId');

//        $statusType = $this->input->post('statusType');

        $status = $this->input->post('status');

        $statusTitle = $this->input->post('statusTitle');

        $startTime = $this->input->post('startTime');

        $timeDuration = $this->input->post('timeDuration');

        $isAvailable = $this->input->post('isAvailable');

        $timeFormat = $this->input->post('timeFormat');

        $hideFrom = $this->input->post('hideFrom');

//        $userId = '34';
//        $statusId = '481';
//        $status = 'Mobile Gaming';
//        $statusTitle = 'hi';
//        $startTime = '30/07/2015';
//        $timeDuration = '20';
//        $isAvailable = '0';
//        $timeFormat = 'hour';
//        print_r($_POST); die();

        if ($_POST) {

            $data = $this->main_manager->select_by_id($statusId, "user_status");

            if ($data) {
                if ($hideFrom) {
                    $data = array(
                        'user_status' => $status,
                        'start_time' => $startTime,
                        'status_title' => $statusTitle,
                        'time_duration' => $timeDuration,
                        'is_available' => $isAvailable,
                        'time_format' => $timeFormat,
                        'hide_from_user_id' => $hideFrom,
                        'status' => 1
                    );
                } else {
                    $data = array(
                        'user_status' => $status,
                        'start_time' => $startTime,
                        'status_title' => $statusTitle,
                        'time_duration' => $timeDuration,
                        'is_available' => $isAvailable,
                        'time_format' => $timeFormat,
                        'hide_from_user_id' => 0,
                        'status' => 1
                    );
                }
                $this->main_manager->update($statusId, $data, 'user_status');

                $finalData['data'] = $this->main_manager->select_by_id($statusId, "user_status");

                $finalData['status'] = 1;
            } else {
                $finalData['message'] = "Status does not exist";
            }
        } else {
            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    # Delete status webservice ...

    public function deleteStatus() {

        $statusId = $this->input->post('statusId');

//        $statusId = "2";

        if ($statusId) {

            $data = $this->main_manager->select_by_id($statusId, "user_status");

            if ($data) {

                $data = array(
                    'status' => 0
                );

                $this->main_manager->update($statusId, $data, 'user_status');

//            $this->main_manager->update($statusId, $data, 'user_status_media');

                $finalData['status'] = 1;
            } else {
                $finalData['message'] = 'Status does not exist';
            }
        } else {
            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    # Upload status media webservice ...

    public function uploadStatusMedia() {

        $userId = $this->input->post('userId');

        $statusId = $this->input->post('statusId');

        $time = date("Y-m-d h:i:s");

        $finalData['status'] = 0;

//        $userId = "2";
//        $statusId = "2";
//
//        $statusMedia[0] = "iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKBAMAAABlIDIAAAAAIVBMVEUAAACgoKCgoKDd3d3Z2dnh4eHs7Ozx8fHm5ubc3NzT09OH3f/7AAAAAnRSTlMAtyOW6xcAAAAsSURBVAjXYxBiYGBQVwDiIgYGtTQFBk3TJgYV5yAGZWMFBhUnBgYtoBxQDQBcfARwK7k9TQAAAABJRU5ErkJggg==";
//        $statusMedia[1] = "iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKBAMAAABlIDIAAAAAIVBMVEUAAACgoKCgoKDd3d3Z2dnh4eHs7Ozx8fHm5ubc3NzT09OH3f/7AAAAAnRSTlMAtyOW6xcAAAAsSURBVAjXYxBiYGBQVwDiIgYGtTQFBk3TJgYV5yAGZWMFBhUnBgYtoBxQDQBcfARwK7k9TQAAAABJRU5ErkJggg==";

        $statusMedia = $this->input->post('statusMediaImage');

        if ($statusMedia) {

            for ($i = 0; $i < count($statusMedia); $i++) {

                #uploading image

                ignore_user_abort(true);

                $fileName = uniqid();

                $file = base64_decode($statusMedia[$i]);

                $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";

                $success = file_put_contents($fullPath, $file);

                $pro_image = $fileName . ".png";

                set_time_limit(0);

                $data = array(
                    'user_status_id_fk' => $statusId,
//                'status_type' => $mediaType,
                    'status_media' => $pro_image,
                    'added_at' => $time,
                    'status' => 1,
                );

                $data1 = $this->main_manager->insert($data, 'user_status_media');
            }

            if ($data1 == true) {
                $finalData['status'] = 1;
            } else {
                $finalData['error'] = "error";
            }
        }

        if (isset($_FILES) && !empty($_FILES['statusMediaVideo'])) {

            $thumbnail = $this->input->post('thumbnail');

            $statusMediaVideo = $_FILES['statusMediaVideo'];

            $files = $_FILES;

            $attachName = "statusMediaVideo";

            $count = count($_FILES['statusMediaVideo']['name']);

            for ($i = 0; $i < $count; $i++) {

                #uploading video

                $title = uniqid();

                $_FILES[$attachName]['name'] = $files[$attachName]['name'][$i];

                $_FILES[$attachName]['type'] = $files[$attachName]['type'][$i];

                $_FILES[$attachName]['tmp_name'] = $files[$attachName]['tmp_name'][$i];

                $_FILES[$attachName]['error'] = $files[$attachName]['error'][$i];

                $_FILES[$attachName]['size'] = $files[$attachName]['size'][$i];

                $fileName = $title . '_' . $_FILES[$attachName]['name'];

                $images[] = $fileName;

                $config['file_name'] = $fileName;

                $configVideo['upload_path'] = './assets/status_media/';

                $configVideo['allowed_types'] = '*';

                $this->load->library('upload', $configVideo);

                $this->upload->initialize($configVideo);

                if (!$this->upload->do_upload($attachName)) {

                    echo $this->upload->display_errors();
                } else {

                    $videoDetails = $this->upload->data();

                    $video_link = $videoDetails['file_name'];
                }

                #uploading thumbnail

                ignore_user_abort(true);

                $fileName = uniqid();

                $file = base64_decode($thumbnail[$i]);

                $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";

                $success = file_put_contents($fullPath, $file);

                $pro_image = $fileName . ".png";

                set_time_limit(0);

                $data2 = array(
                    'user_status_id_fk' => $statusId,
                    'status_media' => $video_link,
                    'thumbnail' => $pro_image,
                    'added_at' => $time,
                    'status' => 1,
                );

                $data3 = $this->main_manager->insert($data2, 'user_status_media');
            }

            if ($data3 == true) {

                $finalData['statusId'] = $statusId;

                $finalData['status'] = 1;
            } else {

                $finalData['error'] = "error";
            }
        }

        echo json_encode($finalData);
    }

    public function uploadStatusMediaOld() {

        $userId = $this->input->post('userId');

        $statusId = $this->input->post('statusId');

        $time = date("Y-m-d h:i:s");

        $finalData['status'] = 0;

//        $userId = "2";
//        $statusId = "2";
//
//        $statusMedia[0] = "iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKBAMAAABlIDIAAAAAIVBMVEUAAACgoKCgoKDd3d3Z2dnh4eHs7Ozx8fHm5ubc3NzT09OH3f/7AAAAAnRSTlMAtyOW6xcAAAAsSURBVAjXYxBiYGBQVwDiIgYGtTQFBk3TJgYV5yAGZWMFBhUnBgYtoBxQDQBcfARwK7k9TQAAAABJRU5ErkJggg==";
//        $statusMedia[1] = "iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKBAMAAABlIDIAAAAAIVBMVEUAAACgoKCgoKDd3d3Z2dnh4eHs7Ozx8fHm5ubc3NzT09OH3f/7AAAAAnRSTlMAtyOW6xcAAAAsSURBVAjXYxBiYGBQVwDiIgYGtTQFBk3TJgYV5yAGZWMFBhUnBgYtoBxQDQBcfARwK7k9TQAAAABJRU5ErkJggg==";

        $statusMedia = $this->input->post('statusMediaImage');

        if ($statusMedia) {

            for ($i = 0; $i < count($statusMedia); $i++) {

                #uploading image

                ignore_user_abort(true);

                $fileName = uniqid();

                $file = base64_decode($statusMedia[$i]);

                $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";

                $success = file_put_contents($fullPath, $file);

                $pro_image = $fileName . ".png";

                set_time_limit(0);

                $data = array(
                    'user_status_id_fk' => $statusId,
//                'status_type' => $mediaType,
                    'status_media' => $pro_image,
                    'added_at' => $time,
                    'status' => 1,
                );

                $data1 = $this->main_manager->insert($data, 'user_status_media');
            }

            if ($data1 == true) {
                $finalData['status'] = 1;
            } else {
                $finalData['error'] = "error";
            }
        }

        if (isset($_FILES) && !empty($_FILES['statusMediaVideo'])) {

            $thumbnail = $this->input->post('thumbnail');

            $statusMediaVideo = $_FILES['statusMediaVideo']['name'];

            $configVideo['upload_path'] = './assets/status_media/';

            $configVideo['allowed_types'] = '*';

            $video_name = $_FILES['statusMedia']['name'];

            $configVideo['file_name'] = $video_name;

            $this->load->library('upload', $configVideo);

            $this->upload->initialize($configVideo);

            if (!$this->upload->do_upload('statusMediaVideo')) {

                echo $this->upload->display_errors();
            } else {

                $videoDetails = $this->upload->data();

                $video_link = $videoDetails['file_name'];
            }

            #uploading thumbnail

            ignore_user_abort(true);

            $fileName = uniqid();

            $file = base64_decode($thumbnail);

            $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";

            $success = file_put_contents($fullPath, $file);

            $pro_image = $fileName . ".png";

            set_time_limit(0);

            $data2 = array(
                'user_status_id_fk' => $statusId,
                'status_media' => $video_link,
                'thumbnail' => $pro_image,
                'added_at' => $time,
                'status' => 1,
            );

            $data3 = $this->main_manager->insert($data2, 'user_status_media');
        }

        if ($data3 == true) {
            $finalData['statusId'] = $statusId;

            $finalData['status'] = 1;
        } else {
            $finalData['error'] = "error";
        }

        echo json_encode($finalData);
    }

    # Edit status media webservice ...

    public function editStatusMedia() {

        $userId = $this->input->post('userId');

        $statusId = $this->input->post('statusId');

        $mediaType = $this->input->post('mediaType');

        $thumbnail = $this->input->post('thumbnail');

        $time = date("Y-m-d h:i:s");

//        $userId = "2";
//        $statusId = "1";
//        $mediaType = "video";
//        $statusMedia = "ab.flv";
//        $thumbnail = "ab";

        if ($_POST) {

            $data = $this->main_manager->select_by_id($statusId, "user_status_media");

            if ($data) {

                if ($mediaType == "image") {

                    $statusMedia = $this->input->post('statusMedia');

                    ignore_user_abort(true);

                    $fileName = uniqid();

                    $file = base64_decode($statusMedia);

                    $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";

                    $success = file_put_contents($fullPath, $file);

                    $pro_image = $fileName . ".png";

                    set_time_limit(0);

                    $data = array(
                        'status_type' => $mediaType,
                        'status_media' => $pro_image,
                        'added_at' => $time,
                        'status' => 1,
                    );
                } else {

                    $statusMedia = $_FILES['statusMedia']['name'];

                    $configVideo['upload_path'] = './assets/status_media/';

                    $configVideo['allowed_types'] = '*';

                    $video_name = $_FILES['statusMedia']['name'];

                    $configVideo['file_name'] = $video_name;

                    $this->load->library('upload', $configVideo);

                    $this->upload->initialize($configVideo);

                    if (!$this->upload->do_upload('statusMedia')) {

                        echo $this->upload->display_errors();
                    } else {

                        $videoDetails = $this->upload->data();

                        $video_link = $videoDetails['file_name'];
                    }

                    if ($thumbnail) {

                        $data = array(
                            'status_type' => $mediaType,
                            'status_media' => $video_link,
                            'thumbnail' => $thumbnail,
                            'added_at' => $time,
                            'status' => 1,
                        );
                    } else {

                        $data = array(
                            'status_type' => $mediaType,
                            'status_media' => $video_link,
                            'added_at' => $time,
                            'status' => 1,
                        );
                    }
                }

                if ($this->main_manager->update($statusId, $data, 'user_status_media')) {

                    $mediaStatusId = $this->db->insert_id();

                    $finalData['statusId'] = $statusId;

                    $finalData['statusMediaId'] = $mediaStatusId;

                    $finalData['status'] = 1;
                }
            } else {
                $finalData['message'] = 'Status does not exist';
            }
        } else {
            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    # Delete status media webservice ...

    public function deleteStatusMedia() {

        $statusId = $this->input->post('statusId');

        if ($statusId) {

            $data = $this->main_manager->select_by_id($statusId, "user_status_media");

            if ($data) {

                $data = array(
                    'status' => 0,
                );

                $this->main_manager->update($statusId, $data, 'user_status_media');

                $finalData['status'] = 1;
            } else {

                $finalData['message'] = 'Status does not exist';
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    # Get all status new webservice ...

    public function getAllStatus() {

        $userId = $this->input->post('userId');
        $fData['friends_status'] = array();
        $faData['friendssStatus'] = array();
//        p($faData['friendssStatus']); die();
        $finalData['allStatus'] = array();
        $data['allStatus2'] = array();
        //      $userId = 92;

        if ($userId) {

            $data['allStatus2'] = $this->main_manager->getAllStatus($userId);


            $fData['friends_status'] = $this->main_manager->friendsStatus($userId);
            $faData['friendssStatus'] = $this->main_manager->friendssStatus($userId);
            if (!empty($fData['friends_status']) || !empty($faData['friendssStatus'])) {
                $finalData['allStatus'] = array_merge($data['allStatus2'], $faData['friendssStatus'], $fData['friends_status']);

                # sorting array by date time ..

                function cmp($a, $b) {
                    return $b["id"] - $a["id"];
                }

                usort($finalData['allStatus'], "cmp");
            } else {
                $finalData['allStatus'] = $data['allStatus2'];
            }
            // echo json_encode($finalData['allStatus']);
            // die();
//            p($fData['friends_status']);
//            die();
//            $max = "";
//            $date = date("Y-m-d H:i");
//             $count = count($fData['friends_status']);
//             for($f=0; $f<$count; $f++){
//                 $finalData['friends_status'][$f]['status_media'] = STATUS_MEDIA . $finalData['friends_status'][$f]['status_media'];
//             }
//             p($friendData);
//              die();
//              for($f=0; $f<count($friendData); $f++){
////                  p($friendData[$f]['invite_to']);
//             $finalData['friends_status'] = $this->main_manager->select_by_other_id("user_id",$friendData[$f]['invite_to'], "user_status");
//
//             for($m=0; $m<count($finalData['friends_status']); $m++){
//                 p($finalData['friends_status'][$m]['id']);
//                 $finalData['friends_status_media'] = $this->main_manager->select_by_other_id("user_status_id_fk",$finalData['friends_status'][$m]['id'], "user_status_media");
//             }
//            }
//            p($finalData['friends_status']); echo "\n";
//             p($finalData['friends_status_media']);
//            die();
            if ($finalData['allStatus']) {

                for ($x = 0; $x < count($finalData['allStatus']); $x++) {

                    $finalData['allStatus'][$x]['status_media'] = $this->main_manager->getAllStatusMedia($finalData['allStatus'][$x]['status_id']);

                    if ($finalData['allStatus'][$x]['status_media']) {

                        for ($z = 0; $z < count($finalData['allStatus'][$x]['status_media']); $z++) {

                            @$count[$finalData['allStatus'][$x]['status_media'][$z]['user_status_id_fk']] ++;

                            if ($finalData['allStatus'][$x]['status_media'][$z]['user_status_id_fk']) {

                                $finalData['allStatus'][$x]['media_count'] = $count[$finalData['allStatus'][$x]['status_media'][$z]['user_status_id_fk']];

                                $finalData['allStatus'][$x]['status_media'][0]['thumbnail_icon'] = $finalData['allStatus'][$x]['status_media'][$z]['thumbnail']; //SITE_URL . "assets/thumbnail/" . $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'];
                            } else {

                                $finalData['allStatus'][$x]['media_count'] = 0;
                            }

                            $finalData['allStatus'][$x]['status_media'][$z]['status_media'] = $finalData['allStatus'][$x]['status_media'][$z]['status_media'];

                            if ($finalData['allStatus'][$x]['status_media'][$z]['thumbnail']) {

                                $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'] = $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'];
                            } else {

                                $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'] = 0;
                            }
                        }
                    } else if (!$finalData['allStatus'][$x]['status_media']) {

                        $finalData['allStatus'][$x]['media_count'] = 0;
                    }
                }

                $finalData['status'] = 1;
            } else {
                $finalData['status'] = 0;
            }
        } else {
            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    # Is available status webservice ...

    public function isAvailable() {

        $userId = $this->input->post('userId');

        $statusId = $this->input->post('statusId');

        $isAvailable = $this->input->post('isAvailable');

//        $userId = "34";
//        $isAvailable = "1";

        if ($_POST) {

            $data = $this->main_manager->selectByStatusId($statusId, "user_status");

            if ($data) {

                $data = array(
                    'is_available' => $isAvailable,
                );

                $this->main_manager->update_by_other_id('id', $statusId, $data, 'user_status');

                $finalData['data'] = $this->main_manager->selectByStatusId($statusId, "user_status");
                # send notification function starts from here

                $getFriends = $this->main_manager->select_by_other_id("user_id", $userId, "friends");
//            p($getFriends);
//            echo $hideFrom;
                $hide = array_filter(explode("|", $finalData['data'][0]['hide_from_user_id']));
//            p($hide);

                $countFriends = count($getFriends);
                $counthideId = count($hide);
//            echo $countFriends. $counthideId;
                for ($a = 0; $a < $countFriends; $a++) {
                    for ($z = 0; $z < $counthideId; $z++) {
//                p(@$hide[$a]);
                        if (@$getFriends[$a]['friend_id'] == $hide[$z]) {
//                echo "<br>hidden";
                            unset($getFriends[$a]);
                        }
                    }
                }
//            p($getFriends);
                $countShowFrnd = count($getFriends);
//            die();
                $registrationIds = array();

                for ($s = 0; $s < $countShowFrnd; $s++) {

                    $getUser = $this->main_manager->select_by_other_id("id", $getFriends[$s]['friend_id'], "users");

//            p($getData);
//             die();

                    $registrationIds[$s] = $getUser[0]['reg_id'];
                }


//                $activeStatus['activeStatus'] = $finalData['data'][0];
//                $activeStatus['activeStatus']['status_media'] = $this->main_manager->select_by_other_id("user_status_id_fk", $activeStatus['activeStatus']['id'], "user_status_media");
//                $activeStatus['activeStatus']['media_count'] = count($activeStatus['activeStatus']['status_media']);
//                if ($activeStatus['activeStatus']['status_media'] == 0) {
//                    $activeStatus['activeStatus']['media_count'] = 0;
//                }
//        p($activeStatus);
//        die();
                $activeStatus['status_id'] = $statusId;
                $fields = array(
                    'registration_ids' => $registrationIds,
                    'content_available' => true,
                    'data' => array(
                        "title" => "Active Status",
                        "body" => $activeStatus,
//                "sound" => "default"
                        'badge' => 0
                    )
                );


                $headers = array
                (
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                );

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

                // curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');

                curl_setopt($ch, CURLOPT_POST, true);

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                $result = curl_exec($ch);

                curl_close($ch);

                // }  // Ending For Loop



                /* Sending Notification End */
                $finalData['status'] = 1;
            } else {

                $finalData['message'] = 'Status does not exist';
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    # personalize status webservice .

    public function personalizeStatus() {

        $userId = $this->input->post('userId');

        $statusType = $this->input->post('statusType');

        $status = $this->input->post('status');

        $userId = '3';

        $statusType = 'personalize';

        $status = 'hello';

        if ($userId) {

            $data = array(
                'user_id' => $userId,
                'user_status' => $status,
                'status_type' => $statusType,
                'status' => 1
            );

            $this->main_manager->insert($data, 'user_status');

            $id = $this->db->insert_id();

            $finalData['data'] = $this->main_manager->select_by_id($id, "user_status");

            $finalData['status'] = 1;
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    # Create Status with media webservice ..

    public function createStatus() {
        ini_set('MAX_EXECUTION_TIME', 5000);

        $userId = $this->input->post('userId');

        $statusType = $this->input->post('statusType');

        $status = $this->input->post('status');

        $statusTitle = $this->input->post('statusTitle');

        $startTime = $this->input->post('startTime');

        $timeDuration = $this->input->post('timeDuration');

        $isAvailable = $this->input->post('isAvailable');

        $timeFormat = $this->input->post('timeFormat');

        $hideFrom = $this->input->post('hideFrom');

        #status media parametters
//        define('UPLOAD_DIR', $_SERVER['DOCUMENT_ROOT'] . '/demo/laleoo/assets/');

        $statusId = $this->input->post('statusId');

        $time = date("Y-m-d h:i:s");

        $finalData['status'] = 0;

        $statusMedia = $this->input->post('statusMediaImage');
        $statusMediaVideo = $this->input->post('statusMediaVideo');



//        $statusMedia[0] = "iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKBAMAAABlIDIAAAAAIVBMVEUAAACgoKCgoKDd3d3Z2dnh4eHs7Ozx8fHm5ubc3NzT09OH3f/7AAAAAnRSTlMAtyOW6xcAAAAsSURBVAjXYxBiYGBQVwDiIgYGtTQFBk3TJgYV5yAGZWMFBhUnBgYtoBxQDQBcfARwK7k9TQAAAABJRU5ErkJggg==";
//            $statusMedia[1] = "iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKBAMAAABlIDIAAAAAIVBMVEUAAACgoKCgoKDd3d3Z2dnh4eHs7Ozx8fHm5ubc3NzT09OH3f/7AAAAAnRSTlMAtyOW6xcAAAAsSURBVAjXYxBiYGBQVwDiIgYGtTQFBk3TJgYV5yAGZWMFBhUnBgYtoBxQDQBcfARwK7k9TQAAAABJRU5ErkJggg==";
        $id = "";

//        $userId = 92;
//        $statusId = 914;
//        $statusType = 'Default';
//        $status = 'playing Game';
//        $statusTitle = 'music';
//        $startTime = '3';
//        $timeDuration = '3';
//        $isAvailable = '2';
//        $timeFormat = '2';

        if ($userId) {
            //!-------------------------------- Expire Old status ------------------------------------------------------------------------!//
            /* ! */ $finalData['data'] = $this->main_manager->getAllStatus($userId);                                              /* ! */
            /* ! */ $data = array('is_expire' => 1);                                                                          /* ! */
            /* ! */ for ($i = 0; $i < count($finalData['data']); $i++) { /* ! */
// /*!*/$this->main_manager->update_by_other_id('user_status_id_fk', $finalData['data'][$i]['id'],$data, "user_status_media"); /*!*/
                /* ! */ $this->main_manager->update_by_other_id('id', $finalData['data'][$i]['id'], $data, "user_status");           /* ! */
                /* ! */
            } /* ! */
            /* ! *///-------------------------------------------------------------------------------------------------------------------///*!*/

            if ($hideFrom) {
                $data = array(
                    'user_id' => $userId,
                    'user_status' => $status,
                    'status_title' => $statusTitle,
                    'start_time' => $startTime,
                    'time_duration' => $timeDuration,
                    'is_available' => $isAvailable,
                    'status_type' => $statusType,
                    'time_format' => $timeFormat,
                    'hide_from_user_id' => $hideFrom,
                    'status' => 1
                );
            } else {
                $data = array(
                    'user_id' => $userId,
                    'user_status' => $status,
                    'status_title' => $statusTitle,
                    'start_time' => $startTime,
                    'time_duration' => $timeDuration,
                    'is_available' => $isAvailable,
                    'status_type' => $statusType,
                    'time_format' => $timeFormat,
                    'hide_from_user_id' => 0,
                    'status' => 1
                );
            }
            if ($status) {

                if (empty($statusId)) {

                    $this->main_manager->insert($data, 'user_status');

                    $id = $this->db->insert_id();

                    $getData = $this->main_manager->selectByStatusId($id, "user_status");
                }
            }


            if(isset($_FILES['statusMediaImage'])) {
                if ($_FILES['statusMediaImage']) {

                    for ($i = 0; $i < count($_FILES['statusMediaImage']['name']); $i++) {

                        $configVideo['overwrite'] = false;

                        $configVideo['upload_path'] = UPLOAD_DIR . "status_images/";

                        $configVideo['allowed_types'] = '*';

                        $this->load->library('upload', $configVideo);

                        $files = $_FILES;
                        $cpt = count($_FILES['statusMediaImage']['name']);
                        for ($i = 0; $i < $cpt; $i++) {
                            $imname = $_FILES['statusMediaImage']['name'][$i];
                            $fileExt = array_pop(explode(".", $imname));
                            $fileName = md5(time() / (mt_rand(1, 10)) + (mt_rand(0, 1000))) . "." . $fileExt;
                            $configVideo['file_name'] = $fileName;

                            $_FILES['userfile']['name'] = $fileName;
                            $_FILES['userfile']['type'] = $files['statusMediaImage']['type'][$i];
                            $_FILES['userfile']['tmp_name'] = $files['statusMediaImage']['tmp_name'][$i];
                            $_FILES['userfile']['error'] = $files['statusMediaImage']['error'][$i];
                            $_FILES['userfile']['size'] = $files['statusMediaImage']['size'][$i];

                            $this->upload->initialize($configVideo);
                            $this->upload->do_upload();

                            if ($statusId) {
                                $data = array(
                                    'user_status_id_fk' => $statusId,
                                    'status_media' => "http://horizontechnologies.com.au/demo/laleoo/assets/status_images/" . $fileName,
                                    'added_at' => $time,
                                    'status' => 1,
                                );
                            } else {

                                $data = array(
                                    'user_status_id_fk' => $id,
                                    'status_media' => "http://horizontechnologies.com.au/demo/laleoo/assets/status_images/" . $fileName,
                                    'added_at' => $time,
                                    'status' => 1,
                                );
                            }

                            $data1 = $this->main_manager->insert($data, 'user_status_media');

                        }

                    }
                }
            }


            if(isset($_FILES['statusMediaVideo'])) {
                if ($_FILES['statusMediaVideo']) {

                    for ($i = 0; $i < count($_FILES['statusMediaVideo']['name']); $i++) {

                        $files = $_FILES;
                        $cpt = count($_FILES['statusMediaVideo']['name']);
                        for ($i = 0; $i < $cpt; $i++) {

                            /*upload videos*/
                            $configVideo['overwrite'] = false;
                            $configVideo['upload_path'] = UPLOAD_DIR . "status_videos/";
                            $configVideo['allowed_types'] = '*';
                            $this->load->library('upload', $configVideo);
                            $imname = $_FILES['statusMediaVideo']['name'][$i];
                            $fileExt = array_pop(explode(".", $imname));
                            $fileName = md5(time() / (mt_rand(1, 10)) + (mt_rand(0, 1000))) . "." . $fileExt;
                            $configVideo['file_name'] = $fileName;
                            $_FILES['userfile']['name'] = $fileName;
                            $_FILES['userfile']['type'] = $files['statusMediaVideo']['type'][$i];
                            $_FILES['userfile']['tmp_name'] = $files['statusMediaVideo']['tmp_name'][$i];
                            $_FILES['userfile']['error'] = $files['statusMediaVideo']['error'][$i];
                            $_FILES['userfile']['size'] = $files['statusMediaVideo']['size'][$i];
                            $this->upload->initialize($configVideo);
                            $this->upload->do_upload();
                            /* upload videos end*/

                            /*upload videos*/
                            $configVideo['overwrite'] = false;
                            $configVideo['upload_path'] = UPLOAD_DIR . "status_thumbs/";
                            $configVideo['allowed_types'] = '*';
                            $this->load->library('upload', $configVideo);
                            $imname = $_FILES['thumbnail']['name'][$i];
                            $fileExt = array_pop(explode(".", $imname));
                            $fileNameT = md5(time() / (mt_rand(1, 10)) + (mt_rand(0, 1000))) . "." . $fileExt;
                            $configVideo['file_name'] = $fileNameT;
                            $_FILES['userfile']['name'] = $fileNameT;
                            $_FILES['userfile']['type'] = $files['thumbnail']['type'][$i];
                            $_FILES['userfile']['tmp_name'] = $files['thumbnail']['tmp_name'][$i];
                            $_FILES['userfile']['error'] = $files['thumbnail']['error'][$i];
                            $_FILES['userfile']['size'] = $files['thumbnail']['size'][$i];
                            $this->upload->initialize($configVideo);
                            $this->upload->do_upload();
                            /* upload videos end*/

                            if ($statusId) {
                                $data = array(
                                    'user_status_id_fk' => $statusId,
                                    'status_media' => "http://horizontechnologies.com.au/demo/laleoo/assets/status_videos/" . $fileName,
                                    'thumbnail' => "http://horizontechnologies.com.au/demo/laleoo/assets/status_thumbs/" . $fileNameT,
                                    'added_at' => $time,
                                    'status' => 1,
                                );
                            } else {

                                $data = array(
                                    'user_status_id_fk' => $id,
                                    'status_media' => "http://horizontechnologies.com.au/demo/laleoo/assets/status_videos/" . $fileName,
                                    'thumbnail' => "http://horizontechnologies.com.au/demo/laleoo/assets/status_thumbs/" . $fileNameT,
                                    'added_at' => $time,
                                    'status' => 1,
                                );
                            }

                            $data1 = $this->main_manager->insert($data, 'user_status_media');

                        }

                    }
                }
            }

            /*if ($statusMediaVideo) {

//                print_r($statusMediaVideo); die();

                $thumbnail = $this->input->post('thumbnail');

//                $statusMediaVideo = $_FILES['statusMediaVideo'];
//                $files = $_FILES;
//
//                $attachName = "statusMediaVideo";

                $count = count($statusMediaVideo);
//                print_r($thumbnail); echo "<br>";
//                print_r($count);echo "<br>";
//                print_r($statusMediaVideo);echo "<br>";
//                die();
                for ($i = 0; $i < $count; $i++) {
//                    print_r($statusMediaVideo[$i]);
                    #uploading video
//                    $title = uniqid();
//
//                    $_FILES[$attachName]['name'] = $files[$attachName]['name'][$i];
//
//                    $_FILES[$attachName]['type'] = $files[$attachName]['type'][$i];
//
//                    $_FILES[$attachName]['tmp_name'] = $files[$attachName]['tmp_name'][$i];
//
//                    $_FILES[$attachName]['error'] = $files[$attachName]['error'][$i];
//
//                    $_FILES[$attachName]['size'] = $files[$attachName]['size'][$i];
//
//                    $fileName = $title . '_' . $_FILES[$attachName]['name'];
//
//                    $images[] = $fileName;
//
//                    $config['file_name'] = $fileName;
//
//                    $configVideo['upload_path'] = './assets/status_media/';
//
//                    $configVideo['allowed_types'] = '*';
//
//                    $this->load->library('upload', $configVideo);
//
//                    $this->upload->initialize($configVideo);
//
//                    if (!$this->upload->do_upload($attachName)) {
//
//                        echo $this->upload->display_errors();
//                    } else {
//
//                        $videoDetails = $this->upload->data();
//
//                        $video_link = $videoDetails['file_name'];
//                    }
//
//                    #uploading thumbnail
//
//                    ignore_user_abort(true);
//
//                    $fileName = uniqid();
//
//                    $file = base64_decode($thumbnail[$i]);
//
//                    $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";
//
//                    $success = file_put_contents($fullPath, $file);
//
//                    $pro_image = $fileName . ".png";
//
//                    set_time_limit(0);

                    if ($statusId) {

                        $data2 = array(
                            'user_status_id_fk' => $statusId,
                            'status_media' => $statusMediaVideo[$i],
                            'thumbnail' => $thumbnail[$i],
                            'added_at' => $time,
                            'status' => 1,
                        );
                    } else {

                        $data2 = array(
                            'user_status_id_fk' => $id,
                            'status_media' => $statusMediaVideo[$i],
                            'thumbnail' => $thumbnail[$i],
                            'added_at' => $time,
                            'status' => 1,
                        );
                    }

                    $data3 = $this->main_manager->insert($data2, 'user_status_media');
                }

                if ($data3 == true) {

//                    if ($statusId) {
//                        $finalData['data'] = $this->main_manager->getStatus($statusId);
//                    } else {
//                        $finalData['data'] = $this->main_manager->getStatus($id);
//                    }

                    if ($statusId) {

                        $getData = $this->main_manager->selectByStatusId($statusId, "user_status");
                    } else {

                        $getData = $this->main_manager->selectByStatusId($id, "user_status");
                    }

                    $finalData['status'] = 1;
                } else {

                    $finalData['error'] = "error";
                }
            }*/
            $finalData['data'] = $getData[0];
            $finalData['status'] = 1;
            $getFriends = $this->main_manager->select_by_other_id("user_id", $userId, "friends");
//            p($getFriends);
//            echo $hideFrom;
            $hide = array_filter(explode("|", $hideFrom));


            $countFriends = count($getFriends);
            $counthideId = count($hide);
//            echo $countFriends. $counthideId;
            for ($a = 0; $a < $countFriends; $a++) {
                for ($z = 0; $z < $counthideId; $z++) {
//                p(@$hide[$a]);
                    if (@$getFriends[$a]['friend_id'] == $hide[$z]) {
//                echo "<br>hidden";
                        unset($getFriends[$a]);
                    }
                }
            }



            $countShowFrnd = count($getFriends);
//            die();
            $registrationIds = array();

            for ($s = 0; $s < $countShowFrnd; $s++) {

                $getUser = $this->main_manager->select_by_other_id("id", $getFriends[$s]['friend_id'], "users");



                $registrationIds[$s] = $getUser[0]['reg_id'];
            }



            if (empty($statusId)) {
                $activeStatus['status_id'] = $id;
            } else {
                $activeStatus['status_id'] = $statusId;
            }


            $fields = array(
                'registration_ids' => $registrationIds,
                'content_available' => true,
                'data' => array(
                    "title" => "Active Status",
                    "body" => $activeStatus,
//                "sound" => "default"
                    'badge' => 0
                )
            );
//           echo json_encode($fields);
//            die();

            $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

            // curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');

            curl_setopt($ch, CURLOPT_POST, true);

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);

            curl_close($ch);



            /* Sending Notification End */
        } else {
            $finalData['status'] = 0;
        }
//         echo json_encode($activeStatus)."<br>";
        echo json_encode($finalData);
    }

    # Edit Status with media webservice ..

    public function editStatusWithMedia() {

        $userId = $this->input->post('userId');

        $statusId = $this->input->post('statusId');

        $statusType = $this->input->post('statusType');

        $status = $this->input->post('status');

        $statusTitle = $this->input->post('statusTitle');

        $startTime = $this->input->post('startTime');

        $timeDuration = $this->input->post('timeDuration');

        $isAvailable = $this->input->post('isAvailable');

        $timeFormat = $this->input->post('timeFormat');

        $statusMedia = $this->input->post('statusMediaImage');

        $statusMediaVideo = $this->input->post('statusMediaVideo');

        $hideFrom = $this->input->post('hideFrom');

        $time = date("Y-m-d h:i:s");

//        $userId = '34';
//        $statusId = '481';
//        $status = 'Mobile Gaming';
//        $statusTitle = 'hi';
//        $startTime = '30/07/2015';
//        $timeDuration = '20';
//        $isAvailable = '0';
//        $timeFormat = 'hour';
//        print_r($_POST); die();

        if ($_POST) {

            $data = $this->main_manager->selectByStatusId($statusId, "user_status");

            if ($data) {
                if ($hideFrom) {
                    $data = array(
                        'user_status' => $status,
                        'start_time' => $startTime,
                        'status_title' => $statusTitle,
                        'time_duration' => $timeDuration,
                        'is_available' => $isAvailable,
                        'time_format' => $timeFormat,
                        'hide_from_user_id' => $hideFrom,
                        'status' => 1
                    );
                } else {
                    $data = array(
                        'user_status' => $status,
                        'start_time' => $startTime,
                        'status_title' => $statusTitle,
                        'time_duration' => $timeDuration,
                        'is_available' => $isAvailable,
                        'time_format' => $timeFormat,
                        'hide_from_user_id' => 0,
                        'status' => 1
                    );
                }
                $this->main_manager->update($statusId, $data, 'user_status');

                $finalData['data'] = $this->main_manager->selectByStatusId($statusId, "user_status");

                $finalData['status'] = 1;
            } else {

                $finalData['message'] = "Status does not exist";
            }

            /*if ($statusMedia) {

                for ($i = 0; $i < count($statusMedia); $i++) {

                    #uploading image
//                    ignore_user_abort(true);
//
//                    $fileName = uniqid();
//
//                    $file = base64_decode($statusMedia[$i]);
//
//                    $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";
//
//                    $success = file_put_contents($fullPath, $file);
//
//                    $pro_image = $fileName . ".png";
//
//                    set_time_limit(0);

                    $data = array(
                        'user_status_id_fk' => $statusId,
//                'status_type' => $mediaType,
                        'status_media' => $statusMedia[$i],
                        'added_at' => $time,
                        'status' => 1,
                    );

                    $data1 = $this->main_manager->insert($data, 'user_status_media');
                }

                if ($data1 == true) {

                    $finalData['data'] = $this->main_manager->selectByStatusId($statusId, "user_status");

                    $finalData['status'] = 1;
                } else {

                    $finalData['error'] = "error";
                }
            }*/
            if(isset($_FILES['statusMediaImage'])) {
                if ($_FILES['statusMediaImage']) {

                    for ($i = 0; $i < count($_FILES['statusMediaImage']['name']); $i++) {

                        $configVideo['overwrite'] = false;

                        $configVideo['upload_path'] = UPLOAD_DIR . "status_images/";

                        $configVideo['allowed_types'] = '*';

                        $this->load->library('upload', $configVideo);

                        $files = $_FILES;
                        $cpt = count($_FILES['statusMediaImage']['name']);
                        for ($i = 0; $i < $cpt; $i++) {
                            $imname = $_FILES['statusMediaImage']['name'][$i];
                            $temp = explode(".", $imname);
                            $fileExt = array_pop($temp);
                            $fileName = "asdasd.jpg";
                            $configVideo['file_name'] = $fileName;

                            $_FILES['userfile']['name'] = $fileName;
                            $_FILES['userfile']['type'] = $files['statusMediaImage']['type'][$i];
                            $_FILES['userfile']['tmp_name'] = $files['statusMediaImage']['tmp_name'][$i];
                            $_FILES['userfile']['error'] = $files['statusMediaImage']['error'][$i];
                            $_FILES['userfile']['size'] = $files['statusMediaImage']['size'][$i];

                            $this->upload->initialize($configVideo);
                            $this->upload->do_upload();

                            if ($statusId) {
                                $data = array(
                                    'user_status_id_fk' => $statusId,
                                    'status_media' => "http://horizontechnologies.com.au/demo/laleoo/assets/status_images/" . $fileName,
                                    'added_at' => $time,
                                    'status' => 1,
                                );
                            } else {

                                $data = array(
                                    'user_status_id_fk' => $id,
                                    'status_media' => "http://horizontechnologies.com.au/demo/laleoo/assets/status_images/" . $fileName,
                                    'added_at' => $time,
                                    'status' => 1,
                                );
                            }

                            $data1 = $this->main_manager->insert($data, 'user_status_media');

                        }

                    }
                }
            }

            if ($statusMediaVideo) {

                $thumbnail = $this->input->post('thumbnail');

//                $statusMediaVideo = $_FILES['statusMediaVideo'];

                $files = $_FILES;

                $attachName = "statusMediaVideo";

                $count = count($statusMediaVideo);

                for ($i = 0; $i < $count; $i++) {

                    #uploading video
//                    $title = uniqid();
//
//                    $_FILES[$attachName]['name'] = $files[$attachName]['name'][$i];
//
//                    $_FILES[$attachName]['type'] = $files[$attachName]['type'][$i];
//
//                    $_FILES[$attachName]['tmp_name'] = $files[$attachName]['tmp_name'][$i];
//
//                    $_FILES[$attachName]['error'] = $files[$attachName]['error'][$i];
//
//                    $_FILES[$attachName]['size'] = $files[$attachName]['size'][$i];
//
//                    $fileName = $title . '_' . $_FILES[$attachName]['name'];
//
//                    $images[] = $fileName;
//
//                    $config['file_name'] = $fileName;
//
//                    $configVideo['upload_path'] = './assets/status_media/';
//
//                    $configVideo['allowed_types'] = '*';
//
//                    $this->load->library('upload', $configVideo);
//
//                    $this->upload->initialize($configVideo);
//
//                    if (!$this->upload->do_upload($attachName)) {
//
//                        echo $this->upload->display_errors();
//                    } else {
//
//                        $videoDetails = $this->upload->data();
//
//                        $video_link = $videoDetails['file_name'];
//                    }
//
//                    #uploading thumbnail
//
//                    ignore_user_abort(true);
//
//                    $fileName = uniqid();
//
//                    $file = base64_decode($thumbnail[$i]);
//
//                    $fullPath = UPLOAD_DIR . "status_media/" . $fileName . ".png";
//
//                    $success = file_put_contents($fullPath, $file);
//
//                    $pro_image = $fileName . ".png";
//
//                    set_time_limit(0);

                    $data2 = array(
                        'user_status_id_fk' => $statusId,
                        'status_media' => $statusMediaVideo[$i],
                        'thumbnail' => $thumbnail[$i],
                        'added_at' => $time,
                        'status' => 1,
                    );

                    $data3 = $this->main_manager->insert($data2, 'user_status_media');
                }

                if ($data3 == true) {

                    $finalData['data'] = $this->main_manager->selectByStatusId($statusId, "user_status");
                    $finalData['status'] = 1;
                } else {

                    $finalData['error'] = "error";
                }
            }
            #send notification start from here
            $getFriends = $this->main_manager->select_by_other_id("user_id", $userId, "friends");
//            p($getFriends);
//            echo $hideFrom;
            $hide = array_filter(explode("|", $hideFrom));
//            p($hide);

            $countFriends = count($getFriends);
            $counthideId = count($hide);
//            echo $countFriends. $counthideId;
            for ($a = 0; $a < $countFriends; $a++) {
                for ($z = 0; $z < $counthideId; $z++) {
//                p(@$hide[$a]);
                    if (@$getFriends[$a]['friend_id'] == $hide[$z]) {
//                echo "<br>hidden";
                        unset($getFriends[$a]);
                    }
                }
            }
//            p($getFriends);
            $countShowFrnd = count($getFriends);
//            die();
            $registrationIds = array();

            for ($s = 0; $s < $countShowFrnd; $s++) {

                $getUser = $this->main_manager->select_by_other_id("id", $getFriends[$s]['friend_id'], "users");

//            p($getData);
//             die();

                $registrationIds[$s] = $getUser[0]['reg_id'];
            }

            $activeStatus['status_id'] = $statusId;
//            $activeStatus['activeStatus'] = $finalData['data'][0];
//            $activeStatus['activeStatus']['status_media'] = $this->main_manager->select_by_other_id("user_status_id_fk", $activeStatus['activeStatus']['id'], "user_status_media");
//            $activeStatus['activeStatus']['media_count'] = count($activeStatus['activeStatus']['status_media']);
//            if ($activeStatus['activeStatus']['status_media'] == 0) {
//                $activeStatus['activeStatus']['media_count'] = 0;
//            }
//        p($activeStatus);
//        die();
            $fields = array(
                'registration_ids' => $registrationIds,
                'content_available' => true,
                'data' => array(
                    "title" => "Active Status",
                    "body" => $activeStatus,
//                "sound" => "default"
                    'badge' => 0
                )
            );


            $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

            // curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');

            curl_setopt($ch, CURLOPT_POST, true);

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);

            curl_close($ch);

            // }  // Ending For Loop
            /* Sending Notification End */
//             if ($result === FALSE) {
//            die('Curl failed: ' . curl_error($ch));
//        }
//        else{
//          echo $result;
//            }
        } else {
            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    # Status expire webservice ..

    public function expireStatus() {

        $userId = $this->input->post('userId');

        $statusId = $this->input->post('statusId');

//        $userId = 23;
//        $statusId =5;

        $data = array(
            'is_expire' => 1,
        );

        if ($userId && $statusId) {

            $data1 = $this->main_manager->update($statusId, $data, "user_status");
            $getData = $this->main_manager->selectByStatusId($statusId, "user_status");
            $data2 = $this->main_manager->update_by_other_id('user_status_id_fk', $statusId, $data, 'user_status_media');
//            p($getData);
//            die();
            if ($data1 == true && $data2 == true) {

                $finalData['status'] = 1;

                # send notification function starts from here

                $getFriends = $this->main_manager->select_by_other_id("user_id", $userId, "friends");
//            p($getFriends);
//            echo $hideFrom;
                $hide = array_filter(explode("|", $getData[0]['hide_from_user_id']));
//            p($hide);

                $countFriends = count($getFriends);
                $counthideId = count($hide);
//            echo $countFriends. $counthideId;
                for ($a = 0; $a < $countFriends; $a++) {
                    for ($z = 0; $z < $counthideId; $z++) {
//                p(@$hide[$a]);
                        if (@$getFriends[$a]['friend_id'] == $hide[$z]) {
//                echo "<br>hidden";
                            unset($getFriends[$a]);
                        }
                    }
                }
//            p($getFriends);
                $countShowFrnd = count($getFriends);
//            die();
                $registrationIds = array();

                for ($s = 0; $s < $countShowFrnd; $s++) {

                    $getUser = $this->main_manager->select_by_other_id("id", $getFriends[$s]['friend_id'], "users");

//            p($getData);
//             die();

                    $registrationIds[$s] = $getUser[0]['reg_id'];
                }

                $activeStatus['status_id'] = $statusId;
//                $activeStatus['activeStatus']['status_media'] = $this->main_manager->select_by_other_id("user_status_id_fk", $activeStatus['activeStatus']['id'], "user_status_media");
//                $activeStatus['activeStatus']['media_count'] = count($activeStatus['activeStatus']['status_media']);
//                if ($activeStatus['activeStatus']['status_media'] == 0) {
//                    $activeStatus['activeStatus']['media_count'] = 0;
//                }
//        p($activeStatus);
//        die();
                $fields = array(
                    'registration_ids' => $registrationIds,
                    'content_available' => true,
                    'data' => array(
                        "title" => "Active Status",
                        "body" => $activeStatus,
//                "sound" => "default"
                        'badge' => 0
                    )
                );


                $headers = array
                (
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                );

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

                // curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');

                curl_setopt($ch, CURLOPT_POST, true);

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                $result = curl_exec($ch);

                curl_close($ch);

                // }  // Ending For Loop



                /* Sending Notification End */
            }
        } else {
            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    # Create Personal Status webservice ...

    public function createPersonalStatus() {

        $userId = $this->input->post('userId');

        $status = $this->input->post('personalStatus');

//        $userId = '3';
//        $status = 'hello';

        if ($userId) {

            $data = $this->main_manager->select_by_id($userId, 'users');

            if ($data) {

                $data = array(
                    'user_id' => $userId,
                    'user_status' => $status,
                );

                $this->main_manager->insert($data, 'status_list');

                $id = $this->db->insert_id();

                $finalData['personalId'] = $id;

                $finalData['personalStatus'] = $status;

                $finalData['status'] = 1;
            } else {

                $finalData['status'] = 0;
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    # Get Personal Status webservice ...

    public function getPersonals() {

        $userId = $this->input->post('userId');

//        $userId = 49;
//        $status = 'hello';
//        $finalData['status'] = 0;

        if ($userId) {

            $data = $this->main_manager->select_by_id($userId, 'users');

            if ($data) {

                $finalData['data'] = $this->main_manager->selectByUserIdDesc('user_id', $userId, 'id', 'desc', 'status_list');

                $finalData['status'] = 1;
            } else {

                $finalData['status'] = 0;
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    # delete Personal Status webservice ...

    public function deletePersonalStatus() {

        $userId = $this->input->post('userId');

        $id = $this->input->post('personalId');

//        $userId = 49;
//        $id = 23;

        $data = $this->main_manager->select_by_id($userId, 'users');

        if ($data) {

            $this->main_manager->delete_by_two_id('id', $id, 'user_id', $userId, 'status_list');

            $finalData['status'] = 1;
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    # Sync Contacts webservice ...

    public function syncContacts() {

        $userId = $this->input->post('userId');

        $info = $this->input->post('info');

//        $userId = 92;
//        $info = '[{"contact_name":"Asad","number":"+923333333333"},{"contact_name":"Hshsjs","number":"222222222"},{"contact_name":"Jjsja","number":"57664644694"},{"contact_name":"Jqoqoak","number":"64637755464"},{"contact_name":"Jsjjsjjssj","number":"3110253453"},{"contact_name":"Sami","number":"03123999335"}]';

        $info = json_decode($info, true);

        $finalData = array();

        $data['data'] = $this->main_manager->select_all('users'); # get all user data

        $finalData['data']['unregistered'] = array();

        $finalData['data']['registered'] = array();

        $totalCount = count($info);

        for ($i = 0; $i < $totalCount; $i++) { # info array loop
            if (substr($info[$i]['number'], 0, 1) == "+") { # finds + sign in numbers
                for ($x = 0; $x < count($data['data']); $x++) { # user data array loop
                    if (
                        $info[$i]['number'] == $data['data'][$x]['country_code'] . $data['data'][$x]['phone_number']        # first condition
                    ) {

                        $finalData['data']['registered'][$i]['contact_name'] = $info[$i]['contact_name'];

                        $finalData['data']['registered'][$i]['number'] = $info[$i]['number'];

                        $reg_noo = str_replace($data['data'][$x]['country_code'], "", $info[$i]['number']);

                        $getData = $this->main_manager->select_by_other_id("phone_number", $data['data'][$x]['phone_number'], "users");

                        $finalData['data']['registered'][$i]['user_id'] = $getData[0]['id'];

                        $finalData['data']['registered'][$i]['image'] = PROF_IMG . $getData[0]['image'];

                        $friend_status = 0;

                        $get = $this->main_manager->select_by_two_id("invite_to", $getData[0]['id'], "invite_by", $userId, "invite_users");
                        $get2 = $this->main_manager->select_by_two_id("invite_to", $userId, "invite_by", $getData[0]['id'], "invite_users");

                        if ($get != 0) {
                            $getFriend = $this->main_manager->select_by_id($get[0]['id'], "invite_users");
                            $friend_status = $getFriend[0]['is_accept_invite'];
                        }elseif($get2 != 0){
                            $getFriend = $this->main_manager->select_by_id($get2[0]['id'], "invite_users");
                            $friend_status = $getFriend[0]['is_accept_invite'];
                        }

                        $finalData['data']['registered'][$i]['friend'] = $friend_status;

                        unset($info[$i]);

                        break;
                    }
                } # end of user data array loop
            } else { # finds 0 in numbers
                # 034512345684
                # 00923333333333
                # 9234526484561
                for ($x = 0; $x < count($data['data']); $x++) { # user data array loop
                    $info[$i]['number'] = ltrim($info[$i]['number'], 0);
                    $data['data'][$x]['country_code'] = ltrim($data['data'][$x]['country_code'], "+");
                    $data['data'][$x]['country_code'] . $data['data'][$x]['phone_number'];
//                    p($data['data'][$x]['country_code']); die();
                    if (
                        $info[$i]['number'] == $data['data'][$x]['phone_number']        # first condition
                        || $info[$i]['number'] == $data['data'][$x]['country_code'] . $data['data'][$x]['phone_number'] # second condition
                    ) {

                        $finalData['data']['registered'][$i]['contact_name'] = $info[$i]['contact_name'];

                        $finalData['data']['registered'][$i]['number'] = $info[$i]['number'];

                        $reg_noo = $finalData['data']['registered'][$i]['number'];

//                        echo $reg_noo;

                        if (substr($finalData['data']['registered'][$i]['number'], 0, 1) == 9) {

                            $reg_noo = substr($finalData['data']['registered'][$i]['number'], 1);
                        }

                        $getData = $this->main_manager->select_by_other_id("phone_number", $data['data'][$x]['phone_number'], "users");

                        $finalData['data']['registered'][$i]['user_id'] = $getData[0]['id'];

                        $finalData['data']['registered'][$i]['image'] = PROF_IMG . $getData[0]['image'];

                        $friend_status = 0;

                        $get = $this->main_manager->select_by_two_id("invite_to", $getData[0]['id'], "invite_by", $userId, "invite_users");
                        $get2 = $this->main_manager->select_by_two_id("invite_to", $userId, "invite_by", $getData[0]['id'], "invite_users");

                        if ($get != 0) {
                            $getFriend = $this->main_manager->select_by_id($get[0]['id'], "invite_users");
                            $friend_status = $getFriend[0]['is_accept_invite'];
                        }elseif($get2 != 0){
                            $getFriend = $this->main_manager->select_by_id($get2[0]['id'], "invite_users");
                            $friend_status = $getFriend[0]['is_accept_invite'];
                        }

                        $finalData['data']['registered'][$i]['friend'] = $friend_status;

                        unset($info[$i]);

                        break;
                    }
                } # end of user data array loop
            }
        } #end of info array loop

        $finalData['data']['registered'] = array_values($finalData['data']['registered']);

        $info = array_values($info);

        for ($x = $i; $x < count($finalData['data']['registered']); $x++) {

            for ($i = 0; $i < count($info); $i++) {

                if (@$info[$i]['number'] == $finalData['data']['registered'][$x]['number']) {

                    $finalData['data']['unregistered'][$i]['contact_name'] = $info[$i]['contact_name'];

                    $finalData['data']['unregistered'][$i]['number'] = $info[$i]['number'];
                }
            }
        }

        $finalData['data']['unregistered'] = array_merge($finalData['data']['unregistered'], $info);

        $finalData['data']['unregistered'] = array_values($finalData['data']['unregistered']);

        echo json_encode($finalData);

        die();
    }

    # Send Friend Request webservice ...

    public function sendFriendReq() {

        $requestBy = $this->input->post('reqBy');

        $requestTo = $this->input->post('reqTo');


        $user = $this->main_manager->select_by_id($requestBy, "users");
//        $requestBy = 107;
//        $requestTo = '[{"frnd_id":"105"}]';

        $requestTo = json_decode($requestTo, true);

//        p($requestTo);
//         die();

        $registrationIds = array();
        $getInviteData = $this->main_manager->select_by_other_id("invite_by", $requestBy, "invite_users");

        for ($a = 0; $a < count($requestTo); $a++) {

            //echo $requestTo[$a]['frnd_id'];

            $getData = $this->main_manager->select_by_other_id("id", $requestTo[$a]['frnd_id'], "users");

            $req_flag = true;
            if($getInviteData != false){
                foreach($getInviteData as $key => $val){
                    if($val['invite_to'] == $requestTo[$a]['frnd_id']){
                        $req_flag = false;
                    }
                }
            }


//            p($getData);
//             die();

            if($req_flag){

                if(strlen($getData[0]['reg_id']) > 2){
                    array_push($registrationIds, $getData[0]['reg_id']);
                    $data = array(
                        'invite_by' => $requestBy,
                        'invite_to' => $requestTo[$a]['frnd_id'],
                        'invite_send_at' => date("Y-m-d H:i:s"),
                        'is_accept_invite' => 2,
                        'pending_status' => 0
                    );
                }else{
                    $data = array(
                        'invite_by' => $requestBy,
                        'invite_to' => $requestTo[$a]['frnd_id'],
                        'invite_send_at' => date("Y-m-d H:i:s"),
                        'is_accept_invite' => 2,
                        'pending_status' => 1
                    );
                }
                $dataIns = $this->main_manager->insert($data, "invite_users");
            }
        }
//die();
        //p($registrationIds);
        //die();
//        define('API_ACCESS_KEY', 'AIzaSyA2EwoPIJXUBvVyYacZksGcE87qZrvVdbo');
//        $fields = array
//            (
//            'registration_ids' => $registrationIds,
//            'content_available' => true,
//            'notification' => array(
//                "body" => $user[0]['f_name'] . "would like to add you as a friend.",
//                "title" => "Friend Request",
//                'sound' => "default"
//            )
//        );
        $final_res = "";

        if(count($registrationIds) > 0){

            $headers = array(
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );

            /* Sending Notification Start */
            $fields = array(
                'registration_ids' => $registrationIds,
                'content_available' => true,
                'notification' => array(
                    "title" => "Request Sent",
                    "body" => $user[0]['f_name'] . " would like to add you as a friend",
                    "sound" => "default"
                )
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
            // curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
            /* Sending notification end */

            // }  // Ending For Loop

            $final_res = json_decode($result);
        }

        $finalData['status'] = 1;
        $finalData['ser_res'] = $final_res;
        $finalData['rqby'] = $requestBy;
        $finalData['rqto'] = $requestTo;
        echo json_encode($finalData);
        die();

    }

    # See friend requests webservice ...

    public function friendList() {
        $userId = $this->input->post('userId');
//        $userId = 108;
//        $users[0]['image'] = "";
        $data['data'] = $this->main_manager->select_by_two_id("invite_to", $userId, "is_accept_invite", 2, "invite_users");
//        p($data['data']);
        if (!empty($data['data'])) {
            $count = count($data['data']);
            for ($a = 0; $a < $count; $a++) {
                $users['users'] = $this->main_manager->select_by_id($data['data'][$a]['invite_by'], "users");
//        p($users['users']);
                $data['data'][$a]['contact_name'] = $users['users'][0]['f_name'] . $users['users'][0]['l_name'];
                $data['data'][$a]['user_id'] = $users['users'][0]['id'];
                $data['data'][$a]['image'] = $users['users'][0]['image'];
                $data['status'] = 1;
            }
        }
//     p($data);
//     die();
        echo json_encode($data);
        die();
    }

    # accept friend request webservice ...

    public function acceptRequest() {

        $requestTo = $this->input->post('reqTo');

        $requestBy = $this->input->post('reqBy');
        $user = $this->main_manager->select_by_id($requestTo, "users");
//        $requestTo = 108;
//        $requestBy = 109;

        $data = $this->main_manager->select_by_two_id("invite_by", $requestBy, "invite_to", $requestTo, "invite_users");

        if ($data == 0) {

            $finalData['status'] = 0;

            die();
        } else {

            $dataIns = array(
                'is_accept_invite' => 1,
                'invite_accept_at' => date("Y-m-d H:i:s")
            );

            $this->main_manager->updateByTwoColumns("invite_by", $requestBy, "invite_to", $requestTo, $dataIns, "invite_users");

            $dataFrnd = array(
                'user_id' => $requestBy,
                'friend_id' => $requestTo,
                'status' => 1
            );
            $dataFrnd2 = array(
                'user_id' => $requestTo,
                'friend_id' => $requestBy,
                'status' => 1
            );
            $this->main_manager->insert($dataFrnd, 'friends');
            $this->main_manager->insert($dataFrnd2, 'friends');
//            $finalData['status'] = 1;

            $registrationIds = "";
            $getData = $this->main_manager->select_by_other_id("id", $requestBy, "users");
//
            for ($i = 0; $i < count($getData); $i++) {
                $reg_id_array[$i] = $getData[$i]['reg_id'];
            }

            $registrationIds = $reg_id_array;

//            $registrationIds = $getData[0]['reg_id'];
//            p($registrationIds);
//            die();
//        define('API_ACCESS_KEY', 'AIzaSyA2EwoPIJXUBvVyYacZksGcE87qZrvVdbo');
//            $fields = array
//                (
//                'registration_ids' => $registrationIds,
//                'content_available' => true,
//                'notification' => array(
//                    "body" => "Need Body",
//                    "title" => "Friend Request",
//                    'sound' => "default"
//                )
//            );
            if(count($registrationIds) > 0){

                $fields = array(
                    'registration_ids' => $registrationIds,
                    'content_available' => true,
                    'notification' => array(
                        "title" => "Request Accepted",
                        "body" => $user[0]['f_name'] . " has accepted your friend request",
                        "sound" => "default"
                    )
                );


                $headers = array
                (
                    'Authorization: key=' . API_ACCESS_KEY,
                    'Content-Type: application/json'
                );

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

                // curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');

                curl_setopt($ch, CURLOPT_POST, true);

                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

                $result = curl_exec($ch);

                curl_close($ch);

                // }  // Ending For Loop



                /* Sending Notification End */

            }
        }
        $finalData['notification'] = "accept";
        $finalData['status'] = 1;
        echo json_encode($finalData);
    }

    # Cancel friend request webservice ...

    public function cancelRequest($requestTo, $requestBy) {

//        $requestTo = $this->input->post('reqTo');
//        $requestBy = $this->input->post('reqBy');
//        $requestTo = 40;
//        $requestBy = 24;

        $data = $this->main_manager->delete_by_two_id("invite_by", $requestBy, "invite_to", $requestTo, "invite_users");

        if ($data == 0) {

            $finalData['status'] = 0;

            die();
        } else {

            $finalData['status'] = 1;
        }

        echo json_encode($finalData);
    }

    # decline request webservice ...

    public function declineRequest() {

        $requestTo = $this->input->post('reqTo');

        $requestBy = $this->input->post('reqBy');

//        $requestTo = 40;
//        $requestBy = 24;

        $data = $this->main_manager->delete_by_two_id("invite_by", $requestBy, "invite_to", $requestTo, "invite_users");

        if ($data == 0) {

            $finalData['status'] = 0;

            die();
        } else {

            $finalData['status'] = 1;
        }

        echo json_encode($finalData);
    }

    # upload profile image webservice ...

    public function uploadImage() {
        $userId = $this->input->post('userId');
//        $userId = 70;
        $proImage = $this->input->post('proImage');
//        $proImage = "iVBORw0KGgoAAAANSUhEUgAAAAYAAAAKBAMAAABlIDIAAAAAIVBMVEUAAACgoKCgoKDd3d3Z2dnh4eHs7Ozx8fHm5ubc3NzT09OH3f/7AAAAAnRSTlMAtyOW6xcAAAAsSURBVAjXYxBiYGBQVwDiIgYGtTQFBk3TJgYV5yAGZWMFBhUnBgYtoBxQDQBcfARwK7k9TQAAAABJRU5ErkJggg==";
        if ($proImage) {
            #uploading image
            ignore_user_abort(true);
            $fileName = uniqid();
            $file = base64_decode($proImage);
            $fullPath = UPLOAD_DIR . "profile_images/" . $fileName . ".png";
            $success = file_put_contents($fullPath, $file);
            $pro_image = $fileName . ".png";
            set_time_limit(0);

            $data = array(
                'image' => $pro_image
            );
            $this->main_manager->update($userId, $data, 'users');
            $finalData['status'] = 1;
        } else {
            $finalData['status'] = 0;
        }
        echo json_encode($finalData);
    }

    # get latest status webservice ...

    public function getLatestStatus() {

        $userId = $this->input->post('userId');
//        $userId = 116;
        $fData['friends_status'] = array();
        $finalData['allStatus'] = array();
        $data['allStatus2'] = array();
        $ex = array();
        if ($userId) {
            $data['allStatus2'] = $this->main_manager->getLatestStatus($userId);
//            if ($data['allStatus2']) {
//                $data['allStatus2'][0]['remaining_time'] = 0;
//            }
//            if ($data['allStatus2'][0]['time_format'] == "day") {
//                $ex = explode(" ", $data['allStatus2'][0]['start_time']);
//
//                if (array_key_exists(2, $ex)) {
//                    date_default_timezone_set($ex[2]); // CDT
//                } else {
//                    date_default_timezone_get();
//                }
//                $info = getdate();
//                $startDate = str_replace("/", "-", $ex[0]); //"2010-09-17";
//                $min = $data['allStatus2'][0]['time_duration'];
//                $expiryDate = date('d-m-Y', strtotime($startDate . " + $min days"));
//                $dt = new DateTime();
//                $currentDate = $dt->format('d-m-Y');
//
//                $date1 = date_create($currentDate);
//                $date2 = date_create($expiryDate);
//
//                $diff = date_diff($date1, $date2);
//                $remainingTime = str_replace("+", "", $diff->format("%R%a days"));
//
//                if ($date2 > $date1) {
//                    $data['allStatus2'][0]['remaining_time'] = $remainingTime;
//                } else {
//                    $statusId = $data['allStatus2'][0]['id'];
//                    $expire = array('is_expire' => 1);
//                    $this->main_manager->update($statusId, $expire, "user_status");
//                }
//            } else if ($data['allStatus2'][0]['time_format'] == "Minute") {
//
//                $ex = explode(" ", $data['allStatus2'][0]['start_time']);
////                p($ex); die();
//                if (array_key_exists(2, $ex)) {
//                    date_default_timezone_set($ex[2]); // CDT
//                } else {
//                    date_default_timezone_get();
//                }
//                $info = getdate();
//                $currentDate = strtotime($info[0]);
//                $currentTime = date('h:i:s', time($currentDate));
//
//                $min = $data['allStatus2'][0]['time_duration'];
//                $minto = date($min);
//                $expiryTime = date('h:i:s', strtotime('+' . $minto . 'minutes', strtotime($ex[1])));
//
//                $date_a = new DateTime($expiryTime); // expiry Time
//                $date_b = new DateTime($currentTime); // current Time
//
//                $interval = date_diff($date_a, $date_b);
//
//                if ($date_b < $date_a) {
//                    $time = $interval->format('%h:%i:%s');
//                    $timeArr = array_reverse(explode(":", $time));
//                    $seconds = 0;
//                    foreach ($timeArr as $key => $value) {
//                        if ($key > 2)
//                            break;
//                        $seconds += pow(60, $key) * $value;
//                    }
//                    $minutes = date("i", $seconds);
//                    $data['allStatus2'][0]['remaining_time'] = $minutes;
//                } else {
//                    $statusId = $data['allStatus2'][0]['id'];
//                    $expire = array('is_expire' => 1);
//                    $this->main_manager->update($statusId, $expire, "user_status");
//                }
//            }
            // print_r($data['allStatus2']); die();
            $fData['friends_status'] = $this->main_manager->AllfriendsLatestStatus($userId);

            if ($fData['friends_status']) {
                $count = count($fData['friends_status']);

                for ($i = 0; $i < $count; $i++) {
                    if ($fData['friends_status'][$i]['status_id'] == "") {
                        unset($fData['friends_status'][$i]);
                        break;
//                        echo "status not found"; die();
                    }
//                    $fData['friends_status'][$i]['remaining_time'] = 0;
//                    if ($fData['friends_status'][$i]['time_format'] == "day") {
//
//                        $ex = explode(" ", $fData['friends_status'][$i]['start_time']);
//
//                        if (array_key_exists(2, $ex)) {
//                            date_default_timezone_set($ex[2]); // CDT
//                        } else {
//                            date_default_timezone_get();
//                        }
//                        $info = getdate();
//                        $startDate = str_replace("/", "-", $ex[0]); //"2010-09-17";
//                        $min = $fData['friends_status'][$i]['time_duration'];
//                        $expiryDate = date('d-m-Y', strtotime($startDate . " + $min days"));
//                        $dt = new DateTime();
//                        $currentDate = $dt->format('d-m-Y');
//
//                        $date1 = date_create($currentDate);
//                        $date2 = date_create($expiryDate);
//
//                        $diff = date_diff($date1, $date2);
//                        $remainingTime = str_replace("+", "", $diff->format("%R%a days"));
//
//                        if ($date2 > $date1) {
//
//                            $fData['friends_status'][$i]['remaining_time'] = $remainingTime;
//                        } else {
//                            $statusId = $fData['friends_status'][$i]['id'];
//                            $expire = array('is_expire' => 1);
//                            $this->main_manager->update($statusId, $expire, "user_status");
//                        }
                    #hide from users
//                        $hideUsers = explode(" ", $fData['friends_status'][$i]['hide_from_user_id']);
//                    p($hideUsers); die();
//                    } else if ($fData['friends_status'][$i]['time_format'] == "Minute") {
//
//                        $ex = explode(" ", $fData['friends_status'][$i]['start_time']);
////                    P($ex);
//                        if (array_key_exists(2, $ex)) {
//                            date_default_timezone_set($ex[2]); // CDT
//                        } else {
//                            date_default_timezone_get();
//                        }
//                        $info = getdate();
//                        $currentDate = strtotime($info[0]);
//                        $currentTime = date('h:i:s', time($currentDate));
////                    echo $currentTime;
//                        $min = $fData['friends_status'][$i]['time_duration'];
//                        $minto = date($min);
//                        if (array_key_exists(1, $ex)) {
//                            $expiryTime = date('h:i:s', strtotime('+' . $minto . 'minutes', strtotime($ex[1])));
//                        }
////                    echo $min."<br>";
////                    echo $currentTime;
//                        $date_a = new DateTime($expiryTime); // expiry Time
//                        $date_b = new DateTime($currentTime); // current Time
//
//                        $interval = date_diff($date_a, $date_b);
////                    p($interval);
//                        if ($date_b < $date_a) {
//                            $time = $interval->format('%h:%i:%s');
//                            $timeArr = array_reverse(explode(":", $time));
//                            $seconds = 0;
//                            foreach ($timeArr as $key => $value) {
//                                if ($key > 2)
//                                    break;
//                                $seconds += pow(60, $key) * $value;
//                            }
//                            $minutes = date("i", $seconds);
//                            $fData['friends_status'][$i]['remaining_time'] = $minutes;
//                        } else {
//                            $statusId = $fData['friends_status'][$i]['status_id'];
//                            $expire = array('is_expire' => 1);
//                            $this->main_manager->update($statusId, $expire, "user_status");
//                        }
                    #hide from users
                    $hideUsers = explode("|", $fData['friends_status'][$i]['hide_from_user_id']);
//              p($hideUsers);
                    $hiddenCount = count($hideUsers);

                    for ($h = 0; $h < $count; $h++) {
                        if (@$hideUsers[$h] == $userId) {
//                          echo @$hideUsers[$h];
                            unset($fData['friends_status'][$i]);
                            break;
                        }
                    }
//                   p($fData['friends_status'][$i]);
                }
            }
        }


        if ($data['allStatus2']){
            $personalId = $this->main_manager->selectPersonalId($data['allStatus2'][0]['user_status'], $data['allStatus2'][0]['user_id']);
            if ($personalId != "") {
                $data['allStatus2'][0]['personal_id'] = $personalId[0]['personal_id'];
            }
            if ($fData['friends_status']) {
                $finalData['allStatus'] = array_merge($data['allStatus2'], $fData['friends_status']);
            }else {
                $finalData['allStatus'] = $data['allStatus2'];
            }
        }elseif($fData['friends_status']){
            $finalData['allStatus'] = $fData['friends_status'];
        }


        if ($finalData['allStatus']) {
            for ($x = 0; $x < count($finalData['allStatus']); $x++) {
                $finalData['allStatus'][$x]['status_media'] = "";
                $finalData['allStatus'][$x]['media_count'] = 0;
            }
            for ($x = 0; $x < count($finalData['allStatus']); $x++) {
                if ($finalData['allStatus'][$x]['status_id']) {
                    $finalData['allStatus'][$x]['status_media'] = $this->main_manager->getAllStatusMedia($finalData['allStatus'][$x]['status_id']);
                    //p($finalData['allStatus'][$x]['status_media']);
//                    die();

                    if ($finalData['allStatus'][$x]['status_media']) {

                        for ($z = 0; $z < count($finalData['allStatus'][$x]['status_media']); $z++) {

                            $count = $finalData['allStatus'][$x]['status_media'][$z]['user_status_id_fk'];
                            // $count = $finalData['allStatus'][$x]['status_media'][$z];
                            //p($count) ;
                            if ($finalData['allStatus'][$x]['status_media'][$z]['user_status_id_fk']) {

                                $finalData['allStatus'][$x]['media_count'] = count($finalData['allStatus'][$x]['status_media']);

                                $finalData['allStatus'][$x]['status_media'][0]['thumbnail_icon'] = $finalData['allStatus'][$x]['status_media'][$z]['thumbnail']; //SITE_URL . "assets/thumbnail/" . $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'];
                            } else {

                                $finalData['allStatus'][$x]['media_count'] = 0;
                            }

                            $finalData['allStatus'][$x]['status_media'][$z]['status_media'] = $finalData['allStatus'][$x]['status_media'][$z]['status_media'];

                            if ($finalData['allStatus'][$x]['status_media'][$z]['thumbnail']) {

                                $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'] = $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'];
                            } else {

                                $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'] = 0;
                            }
                        }
                        //p(array_count_values($count));
                    }
                } else if (!$finalData['allStatus'][$x]['status_media']) {

                    $finalData['allStatus'][$x]['media_count'] = 0;
                }
            }

            $finalData['status'] = 1;
        } else {
            $finalData['status'] = 0;
        }
//        } else {
//            $finalData['status'] = 0;
//        }

        echo json_encode($finalData);
    }

    # get status by id webservice ..

    public function getStatusByStatusId() {

        $statusId = $this->input->post('statusId');
//        $statusId = 1212;
        $finalData['allStatus'] = array();
        $ex = array();
        if ($statusId) {
            $finalData['allStatus'] = $this->main_manager->getStatusByStatusId($statusId);
        }
        $finalData['allStatus'] = $finalData['allStatus'][0];
//            p($data); die();
        if ($finalData['allStatus']) {
//            for ($x = 0; $x < count($finalData['allStatus']); $x++) {
//                $finalData['allStatus'][$x]['status_media'] = "";
//                $finalData['allStatus'][$x]['media_count'] = 0;
//            }
            $finalData['allStatus']['status_media'] = "";
            $finalData['allStatus']['media_count'] = 0;
//            for ($x = 0; $x < count($finalData['allStatus']); $x++) {
            if ($finalData['allStatus']['status_id']) {
                $finalData['allStatus']['status_media'] = $this->main_manager->getAllStatusMedia($finalData['allStatus']['status_id']);

                if ($finalData['allStatus']['status_media']) {

                    for ($z = 0; $z < count($finalData['allStatus']['status_media']); $z++) {

                        $count = $finalData['allStatus']['status_media'][$z]['user_status_id_fk'];
                        // $count = $finalData['allStatus'][$x]['status_media'][$z];
                        //p($count) ;
                        if ($finalData['allStatus']['status_media'][$z]['user_status_id_fk']) {

                            $finalData['allStatus']['media_count'] = count($finalData['allStatus']['status_media']);

                            $finalData['allStatus']['status_media'][0]['thumbnail_icon'] = $finalData['allStatus']['status_media'][$z]['thumbnail']; //SITE_URL . "assets/thumbnail/" . $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'];
                        } else {

                            $finalData['allStatus']['media_count'] = 0;
                        }

                        $finalData['allStatus']['status_media'][$z]['status_media'] = $finalData['allStatus']['status_media'][$z]['status_media'];

                        if ($finalData['allStatus']['status_media'][$z]['thumbnail']) {

                            $finalData['allStatus']['status_media'][$z]['thumbnail'] = $finalData['allStatus']['status_media'][$z]['thumbnail'];
                        } else {

                            $finalData['allStatus']['status_media'][$z]['thumbnail'] = 0;
                        }
                    }
                }
            } else if (!$finalData['allStatus']['status_media']) {

                $finalData['allStatus']['media_count'] = 0;
            }
//            }

            $finalData['status'] = 1;
        } else {
            $finalData['status'] = 0;
        }
        echo json_encode($finalData);
    }

    # get friends list webservice ..

    public function friends() {
        $userId = $this->input->post('userId');
//       $userId = 24;
        #friends data
        $finalData['friends'] = $this->main_manager->getFriends($userId);
        $hiddenIdData = $this->main_manager->getHiddenUserId($userId);
        $finalData['hide_from_user_id'] = $hiddenIdData[0];
        $countFrndData = COUNT($finalData['friends']);
        if ($finalData['friends']) {
            for ($f = 0; $f < $countFrndData; $f++) {
                $finalData['friends'][$f]['image'] = SITE_URL . "assets/profile_images/" . $finalData['friends'][$f]['image'];
            }
        }
//         p($finalData['friends']); die();
        echo json_encode($finalData);
    }

    #Testing functions ...

    public function deleteFriend($myId, $frndId) {
        $getInviteby = $this->main_manager->select_by_two_id("invite_by", $myId, "invite_to", $frndId, "invite_users");
        $getInviteTo = $this->main_manager->select_by_two_id("invite_to", $myId, "invite_by", $frndId, "invite_users");
        $getFrndMyId = $this->main_manager->select_by_two_id("user_id", $myId, "friend_id", $frndId, "friends");
        $getmeFrndId = $this->main_manager->select_by_two_id("friend_id", $myId, "user_id", $frndId, "friends");

        if ($getInviteby[0]) {
            $this->main_manager->delete_by_other_id("invite_by", $myId, "invite_users");
        }if ($getInviteTo[0]) {
            $this->main_manager->delete_by_other_id("invite_to", $myId, "invite_users");
        } if ($getFrndMyId[0]) {
            $this->main_manager->delete_by_other_id("user_id", $myId, "friends");
        } if ($getmeFrndId[0]) {
            $this->main_manager->delete_by_other_id("friend_id", $myId, "friends");
        }
        echo "{status:1}";
    }

    public function getLatestStatusTesting() {

        $userId = $this->input->post('userId');
//        $userId = 116;
        $fData['friends_status'] = array();
        $finalData['allStatus'] = array();
        $data['allStatus2'] = array();
        $ex = array();
        if ($userId) {
            $data['allStatus2'] = $this->main_manager->getLatestStatus($userId);
            if ($data['allStatus2']) {
                $data['allStatus2'][0]['remaining_time'] = 0;
            }
            if ($data['allStatus2'][0]['time_format'] == "day") {
                $ex = explode(" ", $data['allStatus2'][0]['start_time']);

                if (array_key_exists(2, $ex)) {
                    date_default_timezone_set($ex[2]); // CDT
                } else {
                    date_default_timezone_get();
                }
                $info = getdate();
                $startDate = str_replace("/", "-", $ex[0]); //"2010-09-17";
                $min = $data['allStatus2'][0]['time_duration'];
                $expiryDate = date('d-m-Y', strtotime($startDate . " + $min days"));
                $dt = new DateTime();
                $currentDate = $dt->format('d-m-Y');

                $date1 = date_create($currentDate);
                $date2 = date_create($expiryDate);

                $diff = date_diff($date1, $date2);
                $remainingTime = str_replace("+", "", $diff->format("%R%a days"));

                if ($date2 > $date1) {
                    $data['allStatus2'][0]['remaining_time'] = $remainingTime;
                } else {
                    $statusId = $data['allStatus2'][0]['id'];
                    $expire = array('is_expire' => 1);
                    $this->main_manager->update($statusId, $expire, "user_status");
                }
            } else if ($data['allStatus2'][0]['time_format'] == "Minute") {

                $ex = explode(" ", $data['allStatus2'][0]['start_time']);
//                p($ex); die();
                if (array_key_exists(2, $ex)) {
                    date_default_timezone_set($ex[2]); // CDT
                } else {
                    date_default_timezone_get();
                }
                $info = getdate();
                $currentDate = strtotime($info[0]);
                $currentTime = date('h:i:s', time($currentDate));

                $min = $data['allStatus2'][0]['time_duration'];
                $minto = date($min);
                $expiryTime = date('h:i:s', strtotime('+' . $minto . 'minutes', strtotime($ex[1])));

                $date_a = new DateTime($expiryTime); // expiry Time
                $date_b = new DateTime($currentTime); // current Time

                $interval = date_diff($date_a, $date_b);

                if ($date_b < $date_a) {
                    $time = $interval->format('%h:%i:%s');
                    $timeArr = array_reverse(explode(":", $time));
                    $seconds = 0;
                    foreach ($timeArr as $key => $value) {
                        if ($key > 2)
                            break;
                        $seconds += pow(60, $key) * $value;
                    }
                    $minutes = date("i", $seconds);
                    $data['allStatus2'][0]['remaining_time'] = $minutes;
                } else {
                    $statusId = $data['allStatus2'][0]['id'];
                    $expire = array('is_expire' => 1);
                    $this->main_manager->update($statusId, $expire, "user_status");
                }
            }
            // print_r($data['allStatus2']); die();
            $fData['friends_status'] = $this->main_manager->AllfriendsLatestStatus($userId);
//            p($fData['friends_status']); die();
            if ($fData['friends_status']) {
                $count = count($fData['friends_status']);

                for ($i = 0; $i < $count; $i++) {
                    $fData['friends_status'][$i]['remaining_time'] = 0;
                    if ($fData['friends_status'][$i]['time_format'] == "day") {

                        $ex = explode(" ", $fData['friends_status'][$i]['start_time']);

                        if (array_key_exists(2, $ex)) {
                            date_default_timezone_set($ex[2]); // CDT
                        } else {
                            date_default_timezone_get();
                        }
                        $info = getdate();
                        $startDate = str_replace("/", "-", $ex[0]); //"2010-09-17";
                        $min = $fData['friends_status'][$i]['time_duration'];
                        $expiryDate = date('d-m-Y', strtotime($startDate . " + $min days"));
                        $dt = new DateTime();
                        $currentDate = $dt->format('d-m-Y');

                        $date1 = date_create($currentDate);
                        $date2 = date_create($expiryDate);

                        $diff = date_diff($date1, $date2);
                        $remainingTime = str_replace("+", "", $diff->format("%R%a days"));

                        if ($date2 > $date1) {

                            $fData['friends_status'][$i]['remaining_time'] = $remainingTime;
                        } else {
                            $statusId = $fData['friends_status'][$i]['id'];
                            $expire = array('is_expire' => 1);
                            $this->main_manager->update($statusId, $expire, "user_status");
                        }

                        #hide from users
                        $hideUsers = explode(" ", $fData['friends_status'][$i]['hide_from_user_id']);
//                    p($hideUsers); die();
                    } else if ($fData['friends_status'][$i]['time_format'] == "Minute") {

                        $ex = explode(" ", $fData['friends_status'][$i]['start_time']);
//                    P($ex);
                        if (array_key_exists(2, $ex)) {
                            date_default_timezone_set($ex[2]); // CDT
                        } else {
                            date_default_timezone_get();
                        }
                        $info = getdate();
                        $currentDate = strtotime($info[0]);
                        $currentTime = date('h:i:s', time($currentDate));
//                    echo $currentTime;
                        $min = $fData['friends_status'][$i]['time_duration'];
                        $minto = date($min);
                        if (array_key_exists(1, $ex)) {
                            $expiryTime = date('h:i:s', strtotime('+' . $minto . 'minutes', strtotime($ex[1])));
                        }
//                    echo $min."<br>";
//                    echo $currentTime;
                        $date_a = new DateTime($expiryTime); // expiry Time
                        $date_b = new DateTime($currentTime); // current Time

                        $interval = date_diff($date_a, $date_b);
//                    p($interval);
                        if ($date_b < $date_a) {
                            $time = $interval->format('%h:%i:%s');
                            $timeArr = array_reverse(explode(":", $time));
                            $seconds = 0;
                            foreach ($timeArr as $key => $value) {
                                if ($key > 2)
                                    break;
                                $seconds += pow(60, $key) * $value;
                            }
                            $minutes = date("i", $seconds);
                            $fData['friends_status'][$i]['remaining_time'] = $minutes;
                        } else {
                            $statusId = $fData['friends_status'][$i]['id'];
                            $expire = array('is_expire' => 1);
                            $this->main_manager->update($statusId, $expire, "user_status");
                        }
                        #hide from users
                        $hideUsers = explode("|", $fData['friends_status'][$i]['hide_from_user_id']);
//              p($hideUsers);
                        $hiddenCount = count($hideUsers);

                        for ($h = 0; $h < $count; $h++) {
                            if (@$hideUsers[$h] == $userId) {
//                          echo @$hideUsers[$h];
                                unset($fData['friends_status'][$i]);
                                break;
                            }
                        }
//                   p($fData['friends_status'][$i]);
                    }
                }
            }
            if ($data['allStatus2']) {
                if (!empty($fData['friends_status'])) {
                    if ($fData['friends_status']) {
                        $finalData['allStatus'] = array_merge($data['allStatus2'], $fData['friends_status']);
                    }
                } else {
                    $finalData['allStatus'] = $data['allStatus2'];
                }
                # sorting array by date time ..
//                function cmp($a, $b) {
//                    return $b["id"] - $a["id"];
//                }
//
//                usort($finalData['allStatus'], "cmp");
            }
//           echo json_encode($finalData['allStatus']);
//            die();

            if ($finalData['allStatus']) {
                for ($x = 0; $x < count($finalData['allStatus']); $x++) {
                    $finalData['allStatus'][$x]['status_media'] = "";
                    $finalData['allStatus'][$x]['media_count'] = 0;
                }
                for ($x = 0; $x < count($finalData['allStatus']); $x++) {
                    if ($finalData['allStatus'][$x]['status_id']) {
                        $finalData['allStatus'][$x]['status_media'] = $this->main_manager->getAllStatusMedia($finalData['allStatus'][$x]['status_id']);
                        //p($finalData['allStatus'][$x]['status_media']);
//                    die();

                        if ($finalData['allStatus'][$x]['status_media']) {

                            for ($z = 0; $z < count($finalData['allStatus'][$x]['status_media']); $z++) {

                                $count = $finalData['allStatus'][$x]['status_media'][$z]['user_status_id_fk'];
                                // $count = $finalData['allStatus'][$x]['status_media'][$z];
                                //p($count) ;
                                if ($finalData['allStatus'][$x]['status_media'][$z]['user_status_id_fk']) {

                                    $finalData['allStatus'][$x]['media_count'] = count($finalData['allStatus'][$x]['status_media']);

                                    $finalData['allStatus'][$x]['status_media'][0]['thumbnail_icon'] = $finalData['allStatus'][$x]['status_media'][$z]['thumbnail']; //SITE_URL . "assets/thumbnail/" . $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'];
                                } else {

                                    $finalData['allStatus'][$x]['media_count'] = 0;
                                }

                                $finalData['allStatus'][$x]['status_media'][$z]['status_media'] = $finalData['allStatus'][$x]['status_media'][$z]['status_media'];

                                if ($finalData['allStatus'][$x]['status_media'][$z]['thumbnail']) {

                                    $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'] = $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'];
                                } else {

                                    $finalData['allStatus'][$x]['status_media'][$z]['thumbnail'] = 0;
                                }
                            }
                            //p(array_count_values($count));
                        }
                    } else if (!$finalData['allStatus'][$x]['status_media']) {

                        $finalData['allStatus'][$x]['media_count'] = 0;
                    }
                }

                $finalData['status'] = 1;
            } else {
                $finalData['status'] = 0;
            }
        } else {
            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    public function getPersonalStatus($userId) {

//        $userId = 49;
//        $status = 'hello';
//        $finalData['status'] = 0;

        if ($userId) {

            $data = $this->main_manager->select_by_id($userId, 'users');

            if ($data) {

//                $finalData['data'] = $this->main_manager->selectByUserIdDesc('user_id', $userId, 'id', 'desc', 'status_list');
                $finalData['data'] = $this->main_manager->getPersonalStatus($userId);
                $finalData['status'] = 1;
            } else {

                $finalData['status'] = 0;
            }
        } else {

            $finalData['status'] = 0;
        }

        echo json_encode($finalData);
    }

    public function array_count_values_of($value, $array) {

        $counts = array_count_values($array);

        return $counts[$value];
    }

    public function deleteUser($userId) {

        $finalData['data'] = $this->main_manager->getAllStatus($userId);

        $finalData['friends'] = $this->main_manager->select_by_other_id("invite_by", $userId, "invite_users");
        if (!empty($finalData['friends'])) {
            for ($a = 0; $a < count($finalData['friends']); $a++) {
                $finalData['friends'] = $this->main_manager->delete_by_other_id("invite_by", $userId, "invite_users");
            }
        }
        for ($i = 0; $i < count($finalData['data']); $i++) {
            $this->main_manager->delete_by_other_id('user_status_id_fk', $finalData['data'][$i]['id'], "user_status_media");

            $this->main_manager->delete_by_other_id('id', $finalData['data'][$i]['id'], "user_status");
        }

        $this->main_manager->delete_by_other_id('id', $userId, "users");
    }

    public function deleteUserByEmail($email) {

        $finalData['data'] = $this->main_manager->getAllStatus($userId);

        $finalData['friends'] = $this->main_manager->select_by_other_id("invite_by", $userId, "invite_users");
        if (!empty($finalData['friends'])) {
            for ($a = 0; $a < count($finalData['friends']); $a++) {
                $finalData['friends'] = $this->main_manager->delete_by_other_id("invite_by", $userId, "invite_users");
            }
        }
        for ($i = 0; $i < count($finalData['data']); $i++) {
            $this->main_manager->delete_by_other_id('user_status_id_fk', $finalData['data'][$i]['id'], "user_status_media");

            $this->main_manager->delete_by_other_id('id', $finalData['data'][$i]['id'], "user_status");
        }

        $this->main_manager->delete_by_other_id('id', $userId, "users");
    }

    public function deleteUserStatus($userId) {

        $finalData['data'] = $this->main_manager->getAllStatus($userId);

        for ($i = 0; $i < count($finalData['data']); $i++) {
            $this->main_manager->delete_by_other_id('user_status_id_fk', $finalData['data'][$i]['id'], "user_status_media");

            $this->main_manager->delete_by_other_id('id', $finalData['data'][$i]['id'], "user_status");
        }
    }

    public function registration() {

        header('Content-Type: application/json');

        $email = $this->input->post("email");

        $userName = $this->input->post("userName");

        $regId = $this->input->post("regId");

        $type = $this->input->post("type");

        $email = str_replace("%40", "@", $email);

//        $email = "a@a.com";
//        $userName = "A";
//        $regId = "fafsdfs";
//        $type = "Android";

        if ($email != "") {

            $checkemail = $this->main_manager->select_by_other_id("email", $email, "registration");

            if (!empty($checkemail)) {

                $typestrtolower = strtolower($type);

                $data = array(
                    'user_name' => $userName,
                    'reg_id' => $regId,
                    'type' => $type,
                );

                $this->main_manager->update_by_email($checkemail[0]['email'], $data, 'registration');

                $get_data['id'] = $checkemail[0]['id'];

                $get_data['user_name'] = $checkemail[0]['username'];

                $get_data['email'] = $checkemail[0]['email'];

                $get_data['reg_id'] = $regId;

                $get_data['status'] = 1;

                $this->send_notification();

                echo json_encode($get_data);
            } else {

                $data = array(
                    'user_name' => $userName,
                    'email' => $email,
                    'reg_id' => $regId,
                    'type' => $type,
                    'is_verify' => 1,
                );
                $dataIns = array(
                    'f_name' => $f_name,
                    'l_name' => $l_name,
                    'gender' => $gender,
                    'email_address' => $email,
                    'country' => $country,
                    'country_code' => $country_code,
                    'phone_number' => $phone_num,
                    'password' => md5($password),
                    'device_type' => $device_type,
                    'reg_id' => $udid,
                    'status' => '1',
                    'payment_plan_id' => '1',
                    'verification_code' => $rand_taskerurl,
                    'created_at' => date("Y-m-d h:i:s")
                );

                if ($insert_data = $this->main_manager->insert($data, "registration")) {

                    $get = $this->main_manager->select_by_other_id("email", $email, "registration");

                    $get_data['id'] = $get[0]['id'];

                    $get_data['user_name'] = $get[0]['user_name'];

                    $get_data['email'] = $get[0]['email'];

                    $get_data['reg_id'] = $get[0]['reg_id'];

                    $get_data['status'] = 1;

                    $this->send_notification();

                    echo json_encode($get_data);
                }
            }
        }
    }

    public function send_notification() {
//        $regID = array();
        $regID[] = $this->input->post('deviceId');
//        print_r($regID); die();
//        $regID[0]="APA91bGgEQXhWseRgnEBqvi9xV2HqNygThRCsP6jb-TOv6cgUA7czqCra6dlg_HtCoMaHVgr9oGn_qUtYKXjPvkkdoOVd9QhD6SkJ3CTDmuz9RLWSC7b_Ls";
//        $regID[1]="kGhpNGgXnNA:APA91bGBw1iHmgPaOLmMcyFjFhhk8pjU_uSDJlzMyqwcTAZRueQJZ2BO3s293HTf0YBBhDcHz-xkyzp2lgdG_4Wap8Ovl2F7nm_81sz4P13eECSiVB8w8XUc4jBmGZoP_HZj7FyLc0Mt";
        //$final_data['final_data'] = $this->main_manager->select_by_id($id,"registration");
        $final_data['final_data'] = $this->main_manager->select_all("registration");
//        $getData = $this->main_manager->select_by_id($id, "user_status");
//        $activeStatus['activeStatus'] = $getData[0];
//$activeStatus['activeStatus']['status_media'] = $this->main_manager->select_by_other_id("user_status_id_fk",$activeStatus['activeStatus']['id'],"user_status_media");
        /* Sending Notification Start */
//        define('API_ACCESS_KEY', 'AIzaSyA2EwoPIJXUBvVyYacZksGcE87qZrvVdbo');

        $reg_id_array = array();

        for ($i = 0; $i < count($final_data['final_data']); $i++) {

            $reg_id_array[$i] = $final_data['final_data'][$i]["reg_id"];
        }
//        print_r($reg_id_array);
        $registrationIds = $regID;

//        $registrationIds = "[{'mEjwkBc0eLo:APA91bEivlgdsfz_kgh2Xc079CrKAiP9FYYcaV1IoQuofglcGHywVRX_PwdpnP7IWkHDu0bi90CgiPVsQU4kHZCAdC5RhWChD-NEMoiGwHlldZaoPwnPe6-mOerACcw27ZsbLNod7Epu'}]";

        /*
          $fields = array
          (
          'registration_ids' => $registrationIds,
          'content_available' => true,
          'notification' => array(
          "body" => "great match!asdadasd",
          "title" => "Portugal vs. Denmark",
          'sound' => "default"
          )
          ); */
//        $activeStatus['activeStatus']['status_media'] = array();
        $finalData['data'] = $this->main_manager->selectByStatusId(780, "user_status");
        $activeStatus['activeStatus'] = $finalData['data'][0];
        $activeStatus['activeStatus']['status_media'][0]['status_media'] = "http://ikodestudio.com/status";
//     $activeStatus['activeStatus'] = $this->main_manager->select("user_status_id_fk", $activeStatus['activeStatus']['id'], "user_status_media");
//       $activeStatus['activeStatus']['status_media'] = $activeStatus['activeStatus']['status_media'][0]['status_media'];
        $activeStatus['activeStatus']['media_count'] = count($activeStatus['activeStatus']['status_media']);
        if ($activeStatus['activeStatus']['status_media'] == 0) {
            $activeStatus['activeStatus']['media_count'] = 0;
        }

        $fields = array(
            'registration_ids' => $registrationIds,
            'content_available' => true,
            "data" => array(
                "Nickolas" => "Shon",
                "Room" => "PortugalVSusa1",
                "title" => "testt",
                "body" => $activeStatus,
                "sound" => "default"
            ),
            'notification' => array(
                "title" => "aaa",
                "body" => "bbb",
                "sound" => "default"
            )
        );
        echo json_encode($fields);
        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
//        var_dump($fields);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

        //curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');

        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);

        curl_close($ch);

        // }  // Ending For Loop

        /* Sending Notification End */
        if ($result === FALSE) {

            echo $result;

            die('Curl failed: ' . curl_error($ch));
        } else {
//            var_dump($result);
            echo $result;
        }
    }

    public function sendFriendRequest() {
        echo "yes1";
        die("asd");
        $requestBy = $this->input->get('reqBy');

        $requestTo = $this->input->get('reqTo');

        //$requestTo = '[{"frnd_id":"40"},{"frnd_id":"41"},{"frnd_id":"42"},{"frnd_id":"43"},{"frnd_id":"44"}]';

        $requestTo = json_decode($requestTo, true);

        //p($requestTo);

        for ($a = 0; $a < count($requestTo); $a++) {

            $finalData['final_data'][$a] = $this->main_manager->select_by_id($requestTo[$a]['frnd_id'], "users");

            //p($finalData['final_data']);
        }

        /* Sending Notification Start */

//        define('API_ACCESS_KEY', 'AIzaSyA2EwoPIJXUBvVyYacZksGcE87qZrvVdbo');

        $reg_id_array = array();

        for ($i = 0; $i < count($finalData['final_data']); $i++) {

            $reg_id_array[$i] = $finalData['final_data'][$i][0]["reg_id"];
        }

        $registrationIds = $reg_id_array;


        if(count($registrationIds) > 0){
            $fields = array
            (
                'registration_ids' => $registrationIds,
                'content_available' => true,
                'notification' => array(
                    "body" => "great match!",
                    "title" => "Portugal vs. Denmark",
                    'sound' => "default"
                )
            );

            $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

            // curl_setopt($ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send');

            curl_setopt($ch, CURLOPT_POST, true);

            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

            $result = curl_exec($ch);

            curl_close($ch);

            // }  // Ending For Loop

            /* Sending Notification End */

            if ($result === FALSE) {

                echo $result;

                die('Curl failed: ' . curl_error($ch));
            } else {

                echo $result;
            }
        }
    }

    public function register() {

        $f_name = $this->input->post('f_name');

        $l_name = $this->input->post('l_name');

        $gender = $this->input->post('gender');

        $email = $this->input->post('email');

        $country = $this->input->post('country');

        $country_code = $this->input->post('country_code');

        $phone_num = ltrim($this->input->post('phone_num'), 0);

        $password = $this->input->post('password');

        $device_type = $this->input->post('device_type');

        $udid = $this->input->post('regId');

//        $f_name = 'abc';
//        $l_name = 'efg';
//        $gender = 'Male';
//        $email = 'paa@age.com';
//        $country = 'Pakistan';
//        $country_code = "+92";
//        $phone_num = "3361242852";
//        $password = 'PP';
//        $device_type = 'ihpne';
//        $udid = '';

        $getUser = $this->main_manager->getUserData($phone_num, $email);



        if ($getUser) {

            if ($getUser[0]['email_address'] == $email) {

                $final_data['status'] = 0;

                $final_data['error'] = "Email Already Exist";
            }if ($getUser[0]['phone_number'] == $phone_num || $getUser[0]['phone_number'] == 0 . $phone_num) {

                $final_data['status'] = 0;

                $final_data['error'] = "Phone Number Already Exist";
            }if ($getUser[0]['email_address'] == $email && $getUser[0]['phone_number'] == $phone_num) {

                $final_data['status'] = 0;

                $final_data['error'] = "Data Already Exist";
            }

            //////// END HERE////////////////////////
        } else {

            $rand_taskerurl = "abc";

            $is_unique = false;
            // $final_data['user_id'] = $user_id;

            $final_data['status'] = '2';

            $final_data['message'] = 'message sent';

            $dataIns = array(
                'f_name' => $f_name,
                'l_name' => $l_name,
                'gender' => $gender,
                'email_address' => $email,
                'country' => $country,
                'country_code' => $country_code,
                'phone_number' => $phone_num,
                'password' => md5($password),
                'device_type' => $device_type,
                'reg_id' => $udid,
                'status' => '1',
                'payment_plan_id' => '1',
                'verification_code' => $rand_taskerurl,
                'created_at' => date("Y-m-d h:i:s")
            );

            $this->main_manager->insert($dataIns, 'users');

            $user_id = $this->db->insert_id();
            $getUserData = $this->main_manager->getUserDataWithStatusByEmail($email);
            $final_data['user_data'] = $getUserData[0];
            $data[0] = array(
                'user_id' => $user_id,
                'user_status' => "At the Park",
            );

            $data[1] = array(
                'user_id' => $user_id,
                'user_status' => "Relaxing",
            );

            $data[2] = array(
                'user_id' => $user_id,
                'user_status' => "At Work",
            );

            $data[3] = array(
                'user_id' => $user_id,
                'user_status' => "On Vacation",
            );

            for ($o = 0; $o < count($data); $o++) {

                $this->main_manager->insert($data[$o], 'status_list');
            }

//            echo $user_id;
        }

        header("Content-Type: application/json");

        echo json_encode($final_data);

        die();
    }

    public function deleteReq() {

        $rby = $this->input->get('reqBy');
        $rto = $this->input->get('reqTo');

        $this->main_manager->delete_by_other_id('user_id', $rby, "friends");
        $this->main_manager->delete_by_other_id('user_id', $rto, "friends");
        $this->main_manager->delete_by_other_id('invite_by', $rby, "invite_users");

        echo json_encode(array( status => "deleted" ));

    }

}

?>