<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	* Name:  Twilio
	*
	* Author: Ben Edmunds
	*		  ben.edmunds@gmail.com
	*         @benedmunds
	*
	* Location:
	*
	* Created:  03.29.2011
	*
	* Description:  Twilio configuration settings.
	*
	*
	*/

	/**
	 * Mode ("sandbox" or "prod")
	 **/
	$config['mode']   = 'sandbox';

	/**
	 * Account SID 
	 **/
	$config['account_sid']   = 'ACaccfb0efc2ddc548a8bde92b5af0c1c3';

	/**
	 * Auth Token
	 **/
	$config['auth_token']    = 'eccf4dc7973ed89650b9950988d6b5af';

	/**
	 * API Version
	 **/
	$config['api_version']   = '2010-04-01';

	/**
	 * Twilio Phone Number
	 **/
	$config['number']        = '+15674557729';


/* End of file twilio.php */