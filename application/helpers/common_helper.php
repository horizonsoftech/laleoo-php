<?php

function p($array) {
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

function v($array) {
    echo '<pre>';
    var_dump($array);
    echo '</pre>';
}

function redirectSimple($pageName) {
    echo '<script language=javascript>location.href=\'', $pageName, '\';</script>';
    exit;
}

function generate_password() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function uploadImg($file, $path) {
    ignore_user_abort(true);
    $fileName = uniqid();
    $File = base64_decode($file);
    $fullPath = $path . $fileName . ".png";
    $success = file_put_contents($fullPath, $File);
    $pro_image = $fileName . ".png";
    set_time_limit(0);
}

function uploadMultipleImg($file, $path) {
    for ($i = 0; $i < count($file); $i++) {
        #uploading image
        ignore_user_abort(true);
        $fileName = uniqid();
        $File = base64_decode($file[$i]);
        $fullPath = $path . $fileName . ".png";
        $success = file_put_contents($fullPath, $File);
        $pro_image = $fileName . ".png";
        set_time_limit(0);
    }
}
